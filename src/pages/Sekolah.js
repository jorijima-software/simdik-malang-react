import React, {Component} from 'react'

import Shell from '../components/Shell'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Link} from 'react-router-dom'

import {initTable} from "../modules/sekolah";
import {Table, Breadcrumb, Tooltip} from 'antd'

const {Column} = Table

class SekolahPage extends Component {

    componentWillMount() {
        this.props.initTable()
    }

    render() {
        const state = this.props.state
        let pagination = {};
        pagination.total = state.table.total;

        return (
            <Shell>
                <section className="content-header">
                    <h1>Data Sekolah</h1>
                </section>
                <section className="content">
                    <Breadcrumb>
                        <Breadcrumb.Item>Data Sekolah</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="box box-primary" style={{marginTop: '10px'}}>
                        <div className="box-header with-header">
                            <Link className='btn btn-primary btn-sm'
                                  to={`${process.env.PUBLIC_URL}/sekolah/tambah`}>Tambah</Link>
                        </div>
                        <div className="box-body">
                            <Table dataSource={state.table.data} size='small' rowKey={record => record.id}
                                   pagination={pagination}
                                   loading={state.table.loading}
                            >
                                <Column title='NPSN' dataIndex='npsn'/>
                                <Column title='Tingkat' dataIndex='tingkatSekolah'/>
                                <Column title='Nama' dataIndex='nama'/>
                                <Column title='Kecamatan' dataIndex='kecamatan'/>
                                <Column title='Jumlah PTK' dataIndex='totalPegawai'/>
                                <Column title='' dataIndex='id' render={(val, col) => {
                                    return (
                                        <div className='pull-right'>
                                            <Tooltip title='Detail Sekolah'>
                                                <Link className="btn btn-default btn-xs"
                                                      to={`${process.env.PUBLIC_URL}/sekolah/${val}/detail`}
                                                      style={{marginRight: '3px'}}
                                                >
                                                    <i className="fa fa-eye"/>
                                                </Link>
                                            </Tooltip>
                                            <Tooltip title='Reset Password'>
                                                <button className="btn btn-warning btn-xs"
                                                        style={{marginRight: '3px'}}>
                                                    <i className="fa fa-refresh"/>
                                                </button>
                                            </Tooltip>
                                            <Tooltip title='Edit Sekolah'>
                                                <Link className="btn btn-primary btn-xs"
                                                      to={`${process.env.PUBLIC_URL}/sekolah/${val}/edit`}
                                                      style={{marginRight: '3px'}}
                                                >
                                                    <i className="fa fa-pencil"/>
                                                </Link>
                                            </Tooltip>
                                            <Tooltip title='Hapus Sekolah'>
                                                <button className="btn btn-danger btn-xs">
                                                    <i className="fa fa-trash"/>
                                                </button>
                                            </Tooltip>
                                        </div>
                                    )
                                }}/>
                            </Table>
                        </div>
                    </div>
                </section>
            </Shell>
        )
    }
}


const state = state => {
    return {state: state.sekolah}
}

const bindDispatcher = dispatch => bindActionCreators({
    initTable,
}, dispatch)

export default connect(state, bindDispatcher)(SekolahPage)