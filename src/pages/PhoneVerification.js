import React, {Component} from 'react';

import Shell from '../components/Shell';

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';

import logo from '../assets/logo.png';

import {notification} from 'antd';

class PhoneVerification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timer: 300
        }
    }

    componentWillUnmount() {
        notification.destroy()
    }

    componentWillMount() {
        notification.info({
            message: 'Silahkan masukkan kode verifikasi yang telah terkirim ke nomor yang telah terdaftar di aplikasi SIMDIK, atau tunggu beberapa menit untuk melakukan pengiriman kode ulang.',
            duration: 0
        });
    }


    render() {

        return (
            <Shell type='login'>
                <div className="login-box">
                    <div className='row'>
                        <div className="col-sm-2">
                            <img src={logo} alt="Logo Kota Malang" width='60px'/>
                        </div>
                        <div className="col-sm-10">
                            <small>Dinas Pendidikan Kota Malang</small>
                            <div style={{
                                fontFamily: "'Product Sans', sans-serif",
                                fontSize: '24px',
                                fontWeight: '800',
                                marginBottom: '20px'
                            }}>
                                SIMDIK
                            </div>
                        </div>
                    </div>
                    <h1>Verifikasi Telepon</h1>
                    <div className="alert alert-info">
                        Kode verifikasi telah terkirim di nomor: <br/>
                        <span style={{fontWeight: 800, fontSize: 'large'}}
                              className='text-center'>082234347***</span>
                    </div>
                    <div className="login-box-body">

                        <form onSubmit={() => {
                            alert('You\'re submitting, great')
                        }}>
                            <div className="form-group">
                                <label>Kode Verifikasi</label>
                                <input type="text" className="form-control" placeholder='KODE VERIFIKASI'
                                       name='kodeVerifikasi'/>
                            </div>
                            <div className="form-group">
                                <button className="btn btn-block btn-primary">Submit</button>
                                <button className="btn btn-block btn-default">Kirim Ulang</button>
                                <Link to={`${process.env.PUBLIC_URL}/logout`} className='btn btn-block btn-danger'>Logout</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </Shell>
        );
    }

}

const state = state => {
    return {state}
};

const bindDispatcher = dispatch => bindActionCreators({}, dispatch);
export default connect(state, bindDispatcher)(PhoneVerification);