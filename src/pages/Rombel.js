import React, {Component} from 'react'

import Shell from '../components/Shell'
import Statsbox from '../components/Statsbox'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Link} from 'react-router-dom'

import {initRombelSekolah} from "../modules/rombel"

import {Breadcrumb, Table} from 'antd'

const {Column, ColumnGroup} = Table

class RombelPage extends Component {

    componentWillMount() {
        this.props.initRombelSekolah()
    }

    render() {
        const state = this.props.state

        let pagination = {}
        pagination.total = state.tableSekolah.total;

        return (
            <Shell>
                <section className="content-header">
                    <h1>Data Rombel</h1>
                </section>
                <section className="content">
                    <Breadcrumb>
                        <Breadcrumb.Item>Data Rombel</Breadcrumb.Item>
                    </Breadcrumb>
                    <Statsbox data={[{name: 'tesst', value: 600, icon: 'fa-home'}]}/>
                    <div className="box box-primary" style={{marginTop: '10px'}}>
                        <div className="box-body">
                            <Table dataSource={state.tableSekolah.data} size='small'
                                   loading={state.tableSekolah.loading}>
                                <Column title='NPSN' dataIndex='npsn'/>
                                <Column title='Nama' dataIndex='nama' render={(val, col) => {
                                    return <Link to={`${process.env.PUBLIC_URL}/rombel/sekolah/${col.id}`}>{val}</Link>
                                }}/>
                                <Column title='Kecamatan' dataIndex='kecamatan'/>
                                <Column title='Jumlah Rombel' dataIndex='jumlahRombel'/>
                                <ColumnGroup title='Siswa'>
                                    <Column title='Laki' dataIndex='laki' width='8%'/>
                                    <Column title='Perempuan' dataIndex='perempuan' width='8%'/>
                                    <Column title='Total' dataIndex='total' width='8%'/>
                                </ColumnGroup>
                            </Table>
                        </div>
                    </div>
                </section>
            </Shell>
        );
    }
}

const state = state => {
    return {state: state.rombel}
}

const dispatcher = dispatch => bindActionCreators({
    initRombelSekolah
}, dispatch)

export default connect(state, dispatcher)(RombelPage);