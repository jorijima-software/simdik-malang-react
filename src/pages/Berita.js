import React, {Component} from 'react'

import Header from '../components/Header';
import Sidebar from '../components/Sidebar';

import axios from 'axios';

import 'moment/locale/id';
import * as moment from 'moment';

import * as Table from 'reactabular-table';
import {Link} from 'react-router-dom';

import DeleteButton from '../components/DeleteButton';
import Shell from "../components/Shell";

class BeritaPage extends Component {

    constructor() {
        super();
        this.state = {
            berita: {data: [], total: 0}
        };
    }

    componentWillMount() {
        axios.get('//simdik.localhost/api/berita/show?start=0').then(r => {
            if (r.status === 200) {
                this.setState({berita: {data: r.data.data, total: r.data.total}});
            }
        })
    }

    render() {
        const rows = this.state.berita.data.map((item, i) => {
            return {
                $i: i,
                key: item.id,
                no: i + 1,
                judul: item.judul,
                username: (!item.name) ? item.username : item.name,
                date: moment(item.created_at).fromNow()
            }
        });

        const columns = [
            {
                property: 'no',
                header: {
                    label: 'No',
                    transforms: [
                        label => ({
                            onClick: () => alert(`clicked ${label}`)
                        })
                    ]
                }
            },
            {
                property: 'username',
                header: {
                    label: 'Pembuat',
                    transforms: [
                        label => ({
                            onClick: () => alert(`clicked ${label}`)
                        })
                    ]
                }
            },
            {
                property: '$i',
                header: {
                    label: 'Judul',
                    transforms: [
                        label => ({
                            onClick: () => alert(`clicked ${label}`)
                        })
                    ]
                },
                cell: {
                    formatters: [
                        (index) => {
                            const current = rows[index];
                            const key = current.key;
                            const judul = current.judul;
                            return (
                                <div>
                                    <Link to={'/berita/detail/' + key}>{judul}</Link>
                                </div>
                            )
                        }
                    ]
                }
            },
            {
                property: 'date',
                header: {
                    label: 'Waktu',
                    transforms: [
                        label => ({
                            onClick: () => alert(`clicked ${label}`)
                        })
                    ]
                }
            },
            {
                property: 'key',
                cell: {
                    formatters: [
                        (key) => {
                            return (
                                <div className="pull-right">
                                    <Link className="btn btn-primary btn-xs" style={{marginRight: '5px'}}
                                          to={'/berita/edit/' + key}>
                                        <i className="fa fa-pencil"></i> <span>Edit</span>
                                    </Link>
                                    <DeleteButton title='Hapus Berita' onConfirm={alert}/>
                                </div>
                            )
                        }
                    ]
                }
            }
        ];

        return (
            <Shell>
                <section className="content-header">
                    <h1>Berita</h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        <div className="box-header with-border">
                            <div className="box-title">Daftar Berita</div>
                        </div>
                        <div className="box-body">
                            <Table.Provider
                                className="table table-striped table-condensed"
                                columns={columns}
                            >
                                <Table.Header/>

                                <Table.Body rows={rows} rowKey="id"/>
                            </Table.Provider>
                        </div>
                    </div>
                </section>
            </Shell>
        );
    }
}

export default BeritaPage;