import React, {Component} from 'react'

import Header from '../components/Header';
import Sidebar from '../components/Sidebar';

class BackupPage extends Component {
    render() {
        return (
            <div>
                <div className="content-wrapper" style={{height: '100vh'}}>
                    <section className="content-header">
                        <h1>Backup Database</h1>
                    </section>
                </div>
            </div>
        );
    }
}

export default BackupPage;