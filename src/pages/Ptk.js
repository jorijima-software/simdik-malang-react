import React, {Component} from 'react'

import {Link} from 'react-router-dom';

import {fetchPtk, initiateJenisPegawai, deletePtk} from "../modules/ptk";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Breadcrumb, Table, Input, message, Popconfirm} from 'antd';

import Shell from '../components/Shell';

const {Column, ColumnGroup} = Table;
const buttonStyles = {marginRight: '3px'};

const riwayatTpl = (value, col, type) => {
    return <span>
        <Link className='btn btn-default btn-xs'
              style={buttonStyles}
              to={`${process.env.PUBLIC_URL}/ptk/${col.id}/${type}`}>
                <i className="fa fa-eye"/>
        </Link> {value}
        </span>
};

const riwayat = {
    sekolah: (value, col) => (riwayatTpl(value, col, 'sekolah')),
    pangkat: (value, col) => ((col.statusPns != '0') ? riwayatTpl(value, col, 'pangkat') : 'Khusus PNS'),
    gaji: (value, col) => ((col.statusPns != '0') ? riwayatTpl(value, col, 'gaji') : 'Khusus PNS'),
};


class PtkPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            pagination: {},
            params: {search: ''},
            loading: false,
        };

        this.onSearch = this.onSearch.bind(this);
    }


    componentWillMount() {
        this.timer = null;
    }


    onSearch = e => {
        clearTimeout(this.timer);
        const target = e.target;
        const search = target.value;
        this.setState({
            params: {
                ...this.state.params,
                search: search
            }
        });

        this.timer = setTimeout(() => {
            this.fetch(this.state.params)
        }, 350);
    };

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.current = this.props.state.ptk.table.page;

        this.setState({
            pagination: pager,
            params: {...this.state.params, page: pagination.current},
        });

        this.fetch({
            ...this.state.params,
            results: pagination.pageSize,
            page: pagination.current,
            sortField: sorter.field,
            sortOrder: sorter.order,
            ...filters,
        });
    };

    fetch = params => {
        if (params !== undefined) this.setState({params: params});
        setTimeout(() => {
            const page = (this.state.params && this.state.params.page) ? this.state.params.page : ((this.props.state.ptk.table.page) ? this.props.state.ptk.table.page : 1);
            this.props.fetchPtk(page, this.state.params)
        }, 50)
    };

    componentDidMount() {
        this.fetch();
        this.props.initiateJenisPegawai();
    }

    _ptkDelete(id) {
        this.props.deletePtk(id);
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.state.ptk.delete.loading) {
            message.config({top: 200})
            message.loading('Mohon tunggu...', 0)
        } else {
            message.destroy()
            if (nextProps.state.ptk.delete.success) {
                setTimeout(() => {
                    message.config({top: 200})
                    message.success('Sukses menghapus data ptk!', 500)
                }, 100)
                this.fetch(undefined)
            }
        }
    }

    render() {
        const state = this.props.state;
        const pagination = {};
        //
        const jenisPegawaiFilter = state.ptk.jenisPegawai.map((item, i) => {
            return {key: i, text: item.nama, value: item.id}
        });

        const unitKerjaColumn = state.user.level == 0 ?
            <Column title='Unit Kerja'
                    dataIndex='unitKerja'
                    filters={[{
                        text: 'Memiliki Unit Kerja',
                        value: 1
                    }, {text: 'Tidak Memiliki Unit Kerja', value: 0}]}/> : null;

        pagination.total = state.ptk.table.total;
        pagination.current = state.ptk.table.page;

        const actionButton = id => (
            <div className="form-group pull-right">
                <Link to={`${process.env.PUBLIC_URL}/ptk/${id}/detail`} className='btn btn-default btn-xs'
                      style={buttonStyles}>
                    <i className="fa fa-eye"/>
                </Link>
                <Link to={`${process.env.PUBLIC_URL}/ptk/${id}/edit`} className='btn btn-primary btn-xs'
                      style={buttonStyles}>
                    <i className="fa fa-pencil"/></Link>
                <Popconfirm title="Apakah anda yakin ingin menghapus PTK ini?"
                            onConfirm={() => {
                                this._ptkDelete(id)
                            }} okText="Ya"
                            placement='left'
                            cancelText="Batal">
                    <button className='btn btn-danger btn-xs'>
                        <i className="fa fa-trash"/>
                    </button>
                </Popconfirm>
            </div>
        );

        return (
            <Shell>
                <section className="content-header">
                    <h1>Data PTK</h1>
                </section>
                <section className="content">
                    <Breadcrumb>
                        <Breadcrumb.Item>Data PTK</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="box box-primary" style={{marginTop: '10px'}}>
                        <div className="box-body">
                            <div className="pull-right">
                                <Input.Search placeholder='Pencarian' name='search' onChange={this.onSearch}/>
                            </div>
                            <Link to={`${process.env.PUBLIC_URL}/ptk/tambah`}
                                  className='btn btn-primary btn-sm'>Tambah</Link>
                            <hr/>
                            <Table
                                size='small'
                                rowKey={record => record.id}
                                dataSource={state.ptk.table.data}
                                pagination={pagination}
                                loading={state.ptk.table.loading}
                                onChange={this.handleTableChange}
                            >
                                <Column title='NUPTK'
                                        dataIndex='nuptk'
                                        sorter={() => {
                                        }}
                                        filters={[{text: 'Memiliki NUPTK', value: 1}, {
                                            text: 'Tidak Memiliki NUPTK',
                                            value: 0
                                        }]}/>
                                <Column title='Nama'
                                        dataIndex='nama'
                                        sorter={() => {
                                        }}
                                        filters={[{text: 'Guru', value: 1}, {text: 'Sertifikasi', value: 2}]}
                                        render={(v, c) => {
                                            const teacher = (c.guru) ? <i className='fa fa-graduation-cap'></i> : '';
                                            const sertifikasi = (c.sertifikasi) ? <i className="fa fa-star"></i> : '';
                                            return <span className='text-uppercase'><Link
                                                to={`${process.env.PUBLIC_URL}/ptk/${c.id}/detail`}>{v}</Link> {teacher}{sertifikasi}</span>
                                        }}
                                />
                                <Column title='Jenis'
                                        dataIndex='jenisPegawai'
                                        filters={jenisPegawaiFilter}
                                />
                                {unitKerjaColumn}
                                <ColumnGroup title='Riwayat'>
                                    <Column title='Sekolah' dataIndex='count.sekolah' render={riwayat.sekolah}/>
                                    <Column title='Pangkat' dataIndex='count.pangkat' render={riwayat.pangkat}/>
                                    <Column title='Gaji' dataIndex='count.gaji' render={riwayat.gaji}/>
                                </ColumnGroup>
                                <Column dataIndex='id' render={actionButton}/>
                            </Table>
                        </div>
                    </div>
                </section>
            </Shell>
        );
    }
}

const state = state => {
    return {state}
};

const bindDispatcher = dispatch => bindActionCreators({
    fetchPtk,
    initiateJenisPegawai,
    deletePtk
}, dispatch);

export default connect(state, bindDispatcher)(PtkPage);