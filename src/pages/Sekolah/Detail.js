import React, {Component} from 'react'

import Shell from '../../components/Shell'
import InformasiSekolah from '../../components/InformasiSekolah'

import {Breadcrumb} from 'antd'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {detailSekolah} from "../../modules/sekolah";

class SekolahDetailPage extends Component {

    componentWillMount() {
        const params = this.props.match.params;

        this.props.detailSekolah(params.id)
    }

    render() {
        return (
            <Shell>
                <section className="content-header">
                    <h1>Detail Sekolah</h1>
                </section>
                <section className="content">
                    <Breadcrumb>
                        <Breadcrumb.Item>Data Sekolah</Breadcrumb.Item>
                        <Breadcrumb.Item>{'{SEKOLAH_NAMA.GET_UPPERCASE}'}</Breadcrumb.Item>
                    </Breadcrumb>
                    <InformasiSekolah />
                </section>
            </Shell>
        )
    }
}

const state = state => {
    return {
        state: state.sekolah
    }
}

const bindDispatcher = dispatch => bindActionCreators({
    detailSekolah
}, dispatch)

export default connect(state,bindDispatcher)(SekolahDetailPage)