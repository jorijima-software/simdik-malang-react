import React, {Component} from 'react'

import Header from '../components/Header';
import Sidebar from '../components/Sidebar';

import Shell from '../components/Shell'

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {push} from 'react-router-redux';

class DashboardPage extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const state = this.props.state;
        // console.log(state)
        if (!state.login.isLogged) this.props.changePage(`${process.env.PUBLIC_URL}/login`);

        return (
            <Shell>
                <section className="content-header">
                    <h1>Dashboard</h1>
                </section>
            </Shell>
        );
    }
}

const state = state => {
    return {state};
};

const mapDispatch = dispatch => bindActionCreators({
    changePage: where => push(where)
}, dispatch);

export default connect(state, mapDispatch)(DashboardPage)