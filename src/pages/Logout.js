import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {push} from 'react-router-redux';
import {logout} from "../modules/login";
import {message} from "antd/lib/index";

class LogoutPage extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        message.loading('Mohon tunggu...', 0.8);

        // setTimeout(() => {
        setTimeout(() => {
            message.success('Logout sukses', 0.5);
            this.props.logout();
            this.props.changePage(`${process.env.PUBLIC_URL}/login`);
        }, 1000);
    }


    render() {
        return <div></div>;
    }
}

const state = state => {
    return state;
};

const mapDispatcher = dispatch => bindActionCreators({
    logout,
    changePage: where => push(where)
}, dispatch);

export default connect(state, mapDispatcher)(LogoutPage);