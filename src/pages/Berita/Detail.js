import React, {Component} from 'react';

import axios from 'axios';

import Shell from '../../components/Shell';

import 'moment/locale/id';
import * as moment from 'moment';

export default class BeritaDetailPage extends Component {

    constructor() {
        super();
        this.state = {
            berita: {
                judul: null,
                informasi: null,
                nama: null,
                username: null,
                created_at: null
            }
        };
    }

    componentWillMount() {
        const params = this.props.match.params;

        axios.get('//simdik.localhost/api/berita/detail?id=' + params.key).then(_ => {
            this.setState({berita: _.data});
        })
    }

    render() {

        const berita = this.state.berita;
        const date = moment(berita.created_at).fromNow();

        return (
            <Shell>

                <section className="content-header">
                    <h1>Detail Berita</h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        <div className="box-body">
                            <h3 className='berita' style={{margin: '5px 5px 10px 5px'}}>{berita.judul}</h3>
                            <div>
                                <span
                                    style={{margin: '5px'}}>Dibuat Oleh <b>{berita.username}</b> pada <b>{date}</b></span>
                                <hr style={{margin: '5px'}}/>
                                <p className='berita' style={{margin: '5px'}}
                                   dangerouslySetInnerHTML={{__html: berita.informasi}}></p>
                            </div>
                        </div>
                    </div>
                </section>
            </Shell>
        )
    }
}