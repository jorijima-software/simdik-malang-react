import React, {Component} from 'react'

import Shell from '../components/Shell'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {Table, Breadcrumb, Tag} from 'antd'
import {Link} from 'react-router-dom'

import {initTable, fetchTable} from "../modules/guru"

const {Column} = Table

class GuruPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            params: {},
            pagination: {}
        }
        this.handleTableChange = this.handleTableChange.bind(this)
        this.fetch = this.fetch.bind(this)
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.current = this.props.state.table.page;

        this.setState({
            pagination: pager,
            params: {...this.state.params, page: pagination.current},
        });

        this.fetch({
            ...this.state.params,
            results: pagination.pageSize,
            page: pagination.current,
            sortField: sorter.field,
            sortOrder: sorter.order,
            ...filters,
        });
    };

    fetch = params => {
        if (params !== undefined) this.setState({params: params});
        setTimeout(() => {
            const page = (this.state.params && this.state.params.page) ? this.state.params.page : ((this.props.state.ptk.table.page) ? this.props.state.ptk.table.page : 1);
            this.props.fetchTable(page, this.state.params)
        }, 50)
    };

    componentWillMount() {
        this.props.initTable()
    }

    render() {
        const state = this.props.state

        let pagination = {};
        pagination.total = state.table.total;

        return (
            <Shell>
                <section className="content-header">
                    <h1>Data Guru</h1>
                </section>
                <section className="content">
                    <Breadcrumb>
                        <Breadcrumb.Item>Data Guru</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="box box-primary" style={{marginTop: '10px'}}>
                        <div className="box-header with-border">
                            <button className="btn btn-primary">Tambah</button>
                        </div>
                        <div className="box-body">
                            <Table dataSource={state.table.data} size='small' pagination={pagination}
                                   loading={state.table.loading} rowKey={record => record.id}
                                   onChange={this.handleTableChange}
                            >
                                <Column title='NUPTK' dataIndex='nuptk'/>
                                <Column title='Nama' dataIndex='nama' render={(val, col) => {
                                    return <Link to={`${process.env.PUBLIC_URL}/guru/${col.id}/tugas-mengajar`}>{val}</Link>
                                }}/>
                                <Column title='Jenis' dataIndex='jenisPegawai'/>
                                <Column title='Total Jam' dataIndex='jumlahJam' render={(val, col) => {
                                    const label = (val >= 24) ? <Tag color='#87d068'>{val} JAM</Tag> :
                                        <Tag color='#f50'>{val} JAM</Tag>
                                    return <span>
                                        <Link className="btn btn-default btn-xs"
                                              to={`${process.env.PUBLIC_URL}/guru/${col.id}/tugas-mengajar`}>
                                            Detail
                                        </Link> {label}
                                    </span>
                                }}/>
                                <Column title='Tugas Mengajar' dataIndex='tugasMengajar' render={(val) => {
                                    return (val && val.length > 0) ? val : 'Kosong'
                                }}/>
                                <Column title='' dataIndex='id' render={(val) => {
                                    return (
                                        <div className='pull-right'>
                                            <button className="btn btn-danger btn-xs">Hapus
                                            </button>
                                        </div>
                                    )
                                }}/>
                            </Table>
                        </div>
                    </div>
                </section>
            </Shell>
        );
    }
}

const state = state => {
    return {state: state.guru}
}

const dispatcher = dispatch => bindActionCreators({
    initTable,
    fetchTable
}, dispatch)

export default connect(state, dispatcher)(GuruPage);