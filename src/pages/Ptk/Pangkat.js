import React, {Component} from 'react';

import Shell from '../../components/Shell';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'

import {Link} from 'react-router-dom';

import InformasiPtk from '../../components/InformasiPtk';

import {Breadcrumb, Table, Popconfirm} from 'antd';

import {getPangkatPtk, deletePangkatPtk} from "../../modules/ptk";
import {message} from "antd/lib/index";

const {Column} = Table;
const mr = {marginRight: '3px'};

class PtkPangkatPage extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const params = this.props.match.params;
        this.props.getPangkatPtk(params.id);
        this._deletePangkat = this._deletePangkat.bind(this);
    }

    componentDidUpdate() {
        const state = this.props.state;

        if(state.pangkat.delete.loading) {
            message.config({top: 200});
            message.loading('Mohon tunggu...', 0);
        } else {
            message.destroy();
        }
    }

    _deletePangkat(id) {
        const params = this.props.match.params;
        this.props.deletePangkatPtk(params.id, id);
    }

    render() {
        const state = this.props.state;
        const params = this.props.match.params;

        return (
            <Shell>
                <section className="content-header">
                    <h1>Riwayat Pangkat PTK</h1>
                </section>
                <section className="content">
                    <Breadcrumb style={{marginBottom: '10px'}}>
                        <Breadcrumb.Item><Link to={`${process.env.PUBLIC_URL}/ptk`}>Data PTK</Link></Breadcrumb.Item>
                        <Breadcrumb.Item>Riwayat</Breadcrumb.Item>
                        <Breadcrumb.Item>Pangkat</Breadcrumb.Item>
                        <Breadcrumb.Item><span
                            className='text-uppercase'>{state.detail.data.nama_lengkap}</span></Breadcrumb.Item>
                    </Breadcrumb>

                    <InformasiPtk id={params.id}/>

                    <div className="box box-primary">
                        <div className="box-header with-border">
                            <div className="box-title">Riwayat Pangkat</div>
                        </div>
                        <div className="box-body">
                            <Link to={`${process.env.PUBLIC_URL}/ptk/${params.id}/pangkat/tambah`} className='btn btn-primary btn-sm' style={{marginBottom: '10px'}}>Tambah</Link>
                            <Table dataSource={state.pangkat.data} loading={state.pangkat.loading} size='small'
                                   rowKey={col => col.id}>
                                <Column title='No. SK' dataIndex='no_sk'/>
                                <Column title='TMT Golongan' dataIndex='tmt'/>
                                <Column title='Kota' dataIndex='nama_kota' render={(val) => {
                                    return (val && val.length) > 0 ? val : 'KOTA MALANG';
                                }}/>
                                <Column title='Gol/Ruang' dataIndex='gol' render={(val, col) => {
                                    return <span>{val}/{col.ruang}</span>;
                                }}/>
                                <Column title='Jenis' dataIndex='type' render={(val) => {
                                    return (val == '1') ? 'PNS' : 'CPNS';
                                }}/>
                                <Column title='Unit Kerja' dataIndex='nama_sekolah' render={(val, col) => {
                                    return (val && val.length > 0) ? val : col.unit_kerja;
                                }}/>
                                <Column title='' dataIndex='id' render={(val) => {
                                    return (
                                        <div className='pull-right'>
                                            <Link className='btn btn-primary btn-xs' style={mr}
                                                  to={`${process.env.PUBLIC_URL}/ptk/pangkat/${params.id}/edit/${val}`}>Edit</Link>
                                            <Popconfirm title="Apakah anda yakin ingin menghapus sekolah ini?"
                                                        onConfirm={() => {
                                                            this._deletePangkat(val);
                                                        }} placement='left' okText="Ya" cancelText="Tidak">
                                                <button className="btn btn-danger btn-xs">Hapus</button>
                                            </Popconfirm>
                                        </div>
                                    )
                                }}/>
                            </Table>
                        </div>
                    </div>
                </section>
            </Shell>
        );
    }

}

const state = state => {
    return {
        state: {
            detail: state.ptk.detail,
            pangkat: state.ptk.pangkat
        }
    }
};

const bindDispatcher = dispatch => bindActionCreators({
    getPangkatPtk,
    deletePangkatPtk
}, dispatch);

export default connect(state, bindDispatcher)(PtkPangkatPage)