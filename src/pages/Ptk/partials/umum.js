import React from 'react';
import {Form, Input, Select, Spin} from 'antd';

export default function umum(_jenisPegawai, _unitKerja, onUnitKerjaChange, setForm, setSelect, value = {}) {
    const jenisPegawai = _jenisPegawai.map((item, i) => {
        return <Select.Option key={i} value={item.id}>{item.nama}</Select.Option>
    });

    const unitKerja = _unitKerja.map((item, i) => {
        return <Select.Option key={i} value={item.id}>{item.nama}</Select.Option>;
    });

    return (
        <Form>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>NUPTK</strong> :
                </div>
                <div className="col-md-8">
                    <Input placeholder='NUPTK' name="kepegawaian.nuptk" onChange={setForm} value={value.nuptk}/>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Nama</strong> <span className="text-danger">*</span> :
                </div>
                <div className="col-md-8">
                    <Input placeholder='Nama Lengkap' name="kepegawaian.nama" onChange={setForm} value={value.nama}/>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Gelar Depan</strong>
                </div>
                <div className="col-md-3">
                    <Input placeholder='Gelar Depan' name='kepegawaian.gelarDepan' onChange={setForm}
                           value={(value.gelar) ? value.gelar.depan : ''}/>
                </div>
                <div className="col-md-2 text-right">
                    <strong>Gelar Belakang</strong>
                </div>
                <div className="col-md-3">
                    <Input placeholder='Gelar Belakang' name="kepegawaian.gelarBelakang" onChange={setForm}
                           value={(value.gelar) ? value.gelar.belakang : ''}/>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>NIP Lama</strong>
                </div>
                <div className="col-md-3">
                    <Input placeholder='NIP Lama' name='kepegawaian.nipLama' onChange={setForm}
                           value={(value.nip) ? value.nip.lama : ''}/>
                </div>
                <div className="col-md-2 text-right">
                    <strong>NIP Baru</strong>
                </div>
                <div className="col-md-3">
                    <Input placeholder='NIP Baru' name='kepegawaian.nipBaru' onChange={setForm}
                           value={(value.nip) ? value.nip.baru: ''}/>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Jenis Pegawai</strong> <span
                    className="text-danger">*</span> :
                </div>
                <div className="col-md-8">
                    <Select placeholder='Pilih Jenis Pegawai' onChange={value => {
                        setSelect('kepegawaian', 'jenisPegawai', value);
                    }} value={value.idJenisPegawai}>
                        {jenisPegawai}
                    </Select>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Unit Kerja</strong> <span className="text-danger">*</span> :
                </div>
                <div className="col-md-8">
                    <Select showSearch
                            placeholder='Pilih Unit Kerja'
                            onSearch={onUnitKerjaChange}
                            filterOption={false}
                            onChange={value => {
                                setSelect('kepegawaian', 'unitKerja', value);
                            }}
                            value={value.unitKerja}
                    >
                        {unitKerja}
                    </Select>
                </div>
            </Form.Item>
        </Form>
    )
}