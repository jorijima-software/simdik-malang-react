import React from 'react';
import {Form, Input, Select} from 'antd';

export default function (_kota, _kecamatan, onKotaSearch, onKecamatanSearch, setForm, setSelect, value = {}) {
    const kota = _kota.map((item, i) => (
        <Select.Option key={i} value={item.id}>{item.name}</Select.Option>
    ));

    const kecamatan = _kecamatan.map((item, i) => (
        <Select.Option key={i} value={item.id}>{item.name}</Select.Option>
    ));

    return (
        <Form>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Kota/Kecamatan</strong> <span className="text-danger"></span> :
                </div>
                <div className="col-md-8">
                    <Select showSearch
                            placeholder='Pilih atau cari Kota/Kecamatan'
                            onSearch={onKotaSearch}
                            onChange={(value) => {
                                setSelect('rumah', 'kota', value);
                                onKecamatanSearch('');
                            }}
                            filterOption={false}
                            value={value.kota}
                    >
                        {kota}
                    </Select>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Kecamatan</strong> <span className="text-danger"></span> :
                </div>
                <div className="col-md-8">
                    <Select
                        showSearch
                        onSearch={onKecamatanSearch}
                        placeholder='Pilih atau cari Kecamatan'
                        onChange={(value) => {
                            setSelect('rumah', 'kecamatan', value);
                        }}
                        filterOption={false}
                        value={value.kecamatan}
                    >
                        {kecamatan}
                    </Select>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>No. Telp/HP</strong> <span
                    className="text-danger">*</span> :
                </div>
                <div className="col-md-8">
                    <Input placeholder='No. Telp/Handphone' name='rumah.noTelp' onChange={setForm}
                           value={value.noTelp}/>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Alamat</strong> <span className="text-danger"></span> :
                </div>
                <div className="col-md-8">
                    <Input.TextArea placeholder='Alamat Lengkap' name='rumah.alamat' onChange={setForm}
                                    value={value.alamat}/>
                </div>
            </Form.Item>
        </Form>
    );
}