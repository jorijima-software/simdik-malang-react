import React from 'react';
import {Form, Input, Select, DatePicker} from 'antd';

import * as moment from 'moment';

export default function (agama, setForm, setSelect, value = {}) {
    const _agama = agama.map((item, i) => {
        return <Select.Option key={i} value={item.id}>{item.nama}</Select.Option>;
    });

    return (
        <Form>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Tempat Lahir</strong> :
                </div>
                <div className="col-md-8">
                    <Input placeholder='Tempat Lahir' name='profil.tempatLahir' onChange={setForm}
                           value={value.lahir ? value.lahir.tempat : ''}/>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Tanggal Lahir</strong> :
                </div>
                <div className="col-md-8">
                    <DatePicker format='DD/MM/YYYY'
                                placeholder='Tanggal Lahir (dd/mm/yyyy)' onChange={(value) => {
                        setSelect('profil', 'tanggalLahir', value);
                    }}
                                value={value.lahir ? moment(value.lahir.tanggal) : null}
                    />
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Agama</strong> :
                </div>
                <div className="col-md-8">
                    <Select placeholder='Pilih Agama'
                            onChange={(value) => {
                                setSelect('profil', 'agama', value);
                            }}
                            value={value.agama}
                    >
                        {_agama}
                    </Select>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>NIK</strong> <span className="text-danger"></span> :
                </div>
                <div className="col-md-8">
                    <Input placeholder='NIK' name='profil.nik' onChange={setForm} value={value.nik}/>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Nama Ibu</strong> :
                </div>
                <div className="col-md-8">
                    <Input placeholder='Nama Ibu' name='profil.namaIbu' onChange={setForm} value={value.namaIbu}/>
                </div>
            </Form.Item>
        </Form>
    )
}