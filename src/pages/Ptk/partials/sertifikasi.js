import React from 'react';
import {Form, Input, Select} from 'antd';

export default function (_pola, _bidangStudi, onBidangStudiSearch, setForm, setSelect, value = {}) {
    const pola = _pola.map((item, i) => (
        <Select.Option key={i} value={item.key}>{item.nama}</Select.Option>
    ));

    const bidangStudi = _bidangStudi.map((item, i) => (
        <Select.Option key={i} value={item.id}>{item.kode} - {item.nama}</Select.Option>
    ));

    return (
        <Form>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Pola</strong> <span className="text-danger"></span> :
                </div>
                <div className="col-md-8">
                    <Select placeholder='Pilih Pola sertifikasi' onChange={(value) => {
                        setSelect('sertifikasi', 'pola', value);
                    }}
                    value={value.pola}
                    >
                        <Select.Option value='1'>Portofolio</Select.Option>
                        <Select.Option value='2'>PLPG</Select.Option>
                        <Select.Option value='3'>PPG</Select.Option>
                    </Select>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>No. Peserta</strong> <span className="text-danger"></span> :
                </div>
                <div className="col-md-8">
                    <Input placeholder='No. Peserta' name='sertifikasi.noPeserta' onChange={setForm} value={value.noPeserta}/>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Tahun</strong> :
                </div>
                <div className="col-md-8">
                    <Input placeholder='Tahun Sertifikasi' maxLength='4' name='sertifikasi.tahun' onChange={setForm} value={value.tahun}/>
                </div>
            </Form.Item>
            <Form.Item>
                <div className="col-md-2 text-right">
                    <strong>Bidang Studi</strong> :
                </div>
                <div className="col-md-8">
                    <Select
                        showSearch
                        onSearch={onBidangStudiSearch}
                        onChange={(value) => {
                            setSelect('sertifikasi', 'bidangStudi', value);
                        }}
                        placeholder='Pilih atau cari Bidang Studi sertifikasi'
                        filterOption={false}
                        value={value.bidangStudi}
                    >
                        {bidangStudi}
                    </Select>
                </div>
            </Form.Item>
        </Form>
    );
}