import React, {Component} from 'react';

import Shell from '../../components/Shell';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'

import {Link} from 'react-router-dom';

import InformasiPtk from '../../components/InformasiPtk';

import {getGajiPtk} from "../../modules/ptk";

import {Breadcrumb, message, Table, Popconfirm} from 'antd';

import {FormattedNumber, FormattedDate} from 'react-intl'

const {Column} = Table;
const mr = {marginRight: '3px'};

class PtkGajiPage extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const params = this.props.match.params;
        this.props.getGajiPtk(params.id);
    }

    componentDidUpdate() {
        const state = this.props.state;

        // if (state.pangkat.gaji.loading) {
        //     message.config({top: 200});
        //     message.loading('Mohon tunggu...', 0);
        // } else {
        //     message.destroy();
        // }
    }


    render() {
        const state = this.props.state;
        const params = this.props.match.params;

        return (
            <Shell>
                <section className="content-header">
                    <h1>Riwayat Gaji Berkala PTK</h1>
                </section>
                <section className="content">
                    <Breadcrumb style={{marginBottom: '10px'}}>
                        <Breadcrumb.Item><Link to='/ptk'>Data PTK</Link></Breadcrumb.Item>
                        <Breadcrumb.Item>Riwayat</Breadcrumb.Item>
                        <Breadcrumb.Item>Gaji Berkala</Breadcrumb.Item>
                        <Breadcrumb.Item><span
                            className='text-uppercase'>{state.detail.data.nama_lengkap}</span></Breadcrumb.Item>
                    </Breadcrumb>

                    <InformasiPtk id={params.id}/>

                    <div className="box box-primary">
                        <div className="box-header with-border">
                            <div className="box-title">Riwayat Gaji Berkala</div>
                        </div>
                        <div className="box-body">
                            <button className="btn btn-primary btn-sm" style={{marginBottom: '10px'}}>Tambah</button>
                            <Table dataSource={state.gaji.data} rowKey={col => col.id} size='small'>
                                <Column title='No. SK' dataIndex='no_sk'/>
                                <Column title='TMT Golongan' dataIndex='tmt' render={(val) => {
                                    const date = val.split('-');
                                    if(date.length > 0) {
                                        const formattedDate = new Date(date[0], (date[1]-1), date[2]);
                                        return <FormattedDate value={formattedDate}
                                                              year='numeric'
                                                              month='long'
                                                              day='2-digit'/>
                                    }
                                }}/>
                                <Column title='Gol/Ruang' dataIndex='gol' render={(val, col) => {
                                    return <span>{val}/{col.ruang}</span>
                                }}/>
                                <Column title='Jumlah Gaji' dataIndex='jumlah_gaji' render={(val) => {
                                    return (
                                        <FormattedNumber value={val} currency='IDR' style='currency'/>
                                    )
                                }}/>
                                <Column title='' dataIndex='id' render={(val) => {
                                    return (
                                        <div className='text-right'>
                                            <Link className='btn btn-primary btn-xs' style={mr}
                                                  to={`/ptk/gaji/${params.id}/edit/${val}`}>Edit</Link>
                                            <Popconfirm title="Apakah anda yakin ingin menghapus sekolah ini?"
                                                        onConfirm={() => {
                                                            // this._deletePangkat(val);
                                                        }} placement='left' okText="Ya" cancelText="Tidak">
                                                <button className="btn btn-danger btn-xs">Hapus</button>
                                            </Popconfirm>
                                        </div>
                                    )
                                }}/>
                            </Table>
                        </div>
                    </div>
                </section>
            </Shell>
        );
    }

}

const state = state => {
    return {
        state: {
            detail: state.ptk.detail,
            gaji: state.ptk.gaji
        }
    }
};

const bindDispatcher = dispatch => bindActionCreators({
    getGajiPtk
}, dispatch);

export default connect(state, bindDispatcher)(PtkGajiPage)