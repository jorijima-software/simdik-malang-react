import React, {Component} from 'react';
import Shell from '../../components/Shell';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {push, goBack} from 'react-router-redux'

import {Link} from 'react-router-dom';
import {Breadcrumb, Tabs, Spin} from 'antd';

import {getDetailPtk} from "../../modules/ptk";

const TabPane = Tabs.TabPane;

const mr = {marginRight: '3px'};

class PtkDetailPage extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const params = this.props.match.params;
        this.props.getDetailPtk(params.id);
    }


    render() {
        const state = this.props.state;

        if (!state.login.isLogged) this.props.changePage('/login');

        const params = this.props.match.params;

        const detail = state.ptk.detail.data;
        const loading = state.ptk.detail.loading;

        const kepegawaian = detail.kepegawaian ? detail.kepegawaian : {}
        const profil = detail.profil ? detail.profil : {}
        const rumah = detail.rumah ? detail.rumah : {}
        const sertifikasi = detail.sertifikasi ? detail.sertifikasi : {}

        const pola = {
            0: 'PORTOFOLIO',
            1: 'PLPG',
            2: 'PPG'
        };

        return (
            <Shell>
                <section className="content-header">
                    <h1>Detail PTK</h1>
                </section>
                <section className="content">
                    <Breadcrumb style={{marginBottom: '10px'}}>
                        <Breadcrumb.Item><Link to={`${process.env.PUBLIC_URL}/ptk`}>Data PTK</Link></Breadcrumb.Item>
                        <Breadcrumb.Item>Detail</Breadcrumb.Item>
                        <Breadcrumb.Item>{kepegawaian.nama}</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="box box-info">
                        <div className="box-header with-border">
                            <button className="btn btn-sm btn-default" onClick={
                                () => {
                                    this.props.goBack();
                                }
                            } style={mr}>Kembali</button>
                            <Link className="btn btn-info btn-sm" to={`${process.env.PUBLIC_URL}/ptk/${params.id}/sekolah`} style={mr}>Riwayat Sekolah</Link>
                            {(detail.status_pns == '1') ?
                                <span>
                                    <Link className="btn btn-info btn-sm" to={`${process.env.PUBLIC_URL}/ptk/${params.id}/pangkat`} style={mr}>Riwayat Pangkat</Link>
                                    <Link className="btn btn-info btn-sm" to={`${process.env.PUBLIC_URL}/ptk/${params.id}/gaji`} style={mr}>Riwayat Gaji Berkala</Link>
                                </span> : ''
                            }
                        </div>
                        <div className="box-body">
                            <Spin tip="Mohon tunggu..." spinning={loading}>
                                <Tabs defaultActiveKey="1">
                                    <TabPane tab='Kepegawaian'
                                             key="1">
                                        <table className="table table-striped table-hover">
                                            <tbody>
                                            <tr>
                                                <th width="25%" className='text-right'>NUPTK</th>
                                                <td colSpan='3'>{kepegawaian.nuptk}</td>
                                            </tr>
                                            <tr>
                                                <th className='text-right'>Nama</th>
                                                <td colSpan='3'>{kepegawaian.nama}</td>
                                            </tr>
                                            <tr>
                                                <th className='text-right'>Jenis Pegawai</th>
                                                <td colSpan='3'>{kepegawaian.jenisPegawai}</td>
                                            </tr>
                                            <tr>
                                                <th className='text-right'>Unit Kerja</th>
                                                <td colSpan='3' className='text-upperce'>{kepegawaian.unitKerja}</td>
                                            </tr>
                                            <tr>
                                                <th className='text-right' width="25%">NIP Lama</th>
                                                <td width="25%">{kepegawaian.nip ? kepegawaian.nip.lama: ''}</td>
                                                <th className='text-right' width="25%">NIP Baru</th>
                                                <td width="25%">{kepegawaian.nip ? kepegawaian.nip.baru: ''}</td>
                                            </tr>
                                            <tr>
                                                <th className='text-right' width="25%">Gelar Depan</th>
                                                <td width="25%">{kepegawaian.gelar ? kepegawaian.gelar.depan: ''}</td>
                                                <th className='text-right' width="25%">Gelar Belakang</th>
                                                <td width="25%">{kepegawaian.gelar ? kepegawaian.gelar.belakang: ''}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </TabPane>
                                    <TabPane tab='Profil' key='2'>
                                        <table className="table table-striped table-hover">
                                            <tbody>
                                            <tr>
                                                <th width="25%" className='text-right'>NIK</th>
                                                <td>{profil.nik}</td>
                                            </tr>
                                            <tr>
                                                <th className='text-right'>Tempat Lahir</th>
                                                <td>{profil.lahir ? profil.lahir.tempat: ''}</td>
                                            </tr>
                                            <tr>
                                                <th className='text-right'>Tanggal Lahir</th>
                                                <td>{profil.lahir ? profil.lahir.tanggal: ''}</td>
                                            </tr>
                                            <tr>
                                                <th className='text-right'>Agama</th>
                                                <td className='text-uppercase'>{profil.agama}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </TabPane>
                                    <TabPane tab='Rumah' key='3'>
                                        <table className="table table-striped table-hover">
                                            <tbody>
                                            <tr>
                                                <th width="25%" className='text-right'>Kota</th>
                                                <td>{rumah.kota}</td>
                                            </tr>
                                            <tr>
                                                <th width="25%" className='text-right'>Kecamatan</th>
                                                <td>{rumah.kecamatan}</td>
                                            </tr>
                                            <tr>
                                                <th width="25%" className='text-right'>Alamat</th>
                                                <td>{rumah.alamat}</td>
                                            </tr>
                                            <tr>
                                                <th width="25%" className='text-right'>No. Telp / HP</th>
                                                <td>{rumah.noHp}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </TabPane>
                                    <TabPane tab='Sertifikasi' key='4'>
                                        <table className="table table-striped table-hover">
                                            <tbody>
                                            <tr>
                                                <th width="25%" className='text-right'>Pola</th>
                                                <td>{sertifikasi.pola ? pola[sertifikasi.pola] : ''}</td>
                                            </tr>
                                            <tr>
                                                <th width="25%" className='text-right'>No. Peserta</th>
                                                <td>{sertifikasi.noPeserta ? sertifikasi.noPeserta : ''}</td>
                                            </tr>
                                            <tr>
                                                <th width="25%" className='text-right'>Tahun Sertifikasi</th>
                                                <td>{sertifikasi.tahun ? sertifikasi.tahun : ''}</td>
                                            </tr>
                                            <tr>
                                                <th width="25%" className='text-right'>Bidang Studi</th>
                                                <td className='text-uppercase'>{sertifikasi.bidangStudi ? sertifikasi.bidangStudi : ''}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </TabPane>
                                </Tabs>
                            </Spin>
                        </div>
                    </div>
                </section>
            </Shell>
        );
    }
}

const state = state => {
    return {state}
};

const bindDispatcher = dispatch => bindActionCreators({
    getDetailPtk,
    goBack,
    changePage: (where) => push(where)
}, dispatch);

export default connect(state, bindDispatcher)(PtkDetailPage);