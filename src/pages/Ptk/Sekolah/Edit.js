import React, {Component} from 'react'

import Shell from '../../../components/Shell'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Link, Prompt} from 'react-router-dom'
import {push, goBack} from 'react-router-redux';

import {Breadcrumb, Select, Input, Form, message, Alert} from 'antd'

import {getDetailPtk, initiateTingkatPendidikan, setFormSekolah, submitFormSekolah, resetFormSekolah} from '../../../modules/ptk'
import {initiateKota, initiateKecamatan, fetchKota, fetchKecamatan} from "../../../modules/area";

class PtkSekolahTambahPage extends Component {
    constructor(props) {
        super(props);

        this.state = {clean: true};

        this._onKotaSelect = this._onKotaSelect.bind(this);
        this._onKotaSearch = this._onKotaSearch.bind(this);
        this._onKecamatanSearch = this._onKecamatanSearch.bind(this);
        this._setForm = this._setForm.bind(this);
    };

    componentWillMount() {
        const params = this.props.match.params;
        this.props.getDetailPtk(params.id);
        this.props.initiateTingkatPendidikan();
        this.props.initiateKota();
    }

    _onKotaSelect(id) {
        this.props.setFormSekolah('kota', id);
        this.props.initiateKecamatan(id);
    }

    _onKotaSearch(value) {
        if (value.length > 0) {
            this.props.fetchKota(value);
        }
    }

    _onKecamatanSearch(value) {
        const kota = this.props.state.sekolah.form.kota;
        if (value.length > 0) {
            this.props.fetchKecamatan(kota, value);
        }
    }

    _setForm(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({clean: false});
        this.props.setFormSekolah(name, value);
    }

    _onFormSubmit() {
        const state = this.props.state;
        const form = state.sekolah.form;
        const params = this.props.match.params;

        this.props.submitFormSekolah(params.id, form);
    }

    componentWillUpdate() {
        setTimeout(() => {
            const state = this.props.state;
            const params = this.props.match.params;

            if (state.sekolah.form.sending) {
                message.config({top: 150});
                message.loading('Mohon tunggu...', 0);
            } else {
                message.destroy();
            }

            if (state.sekolah.form.success) {
                if (!this.state.clean) this.setState({clean: true});
                setTimeout(() => {
                    message.config({top: 150});
                    message.success('Data sekolah ptk berhasil ditambahkan', 1.5);
                    this.props.resetFormSekolah();
                    this.props.push(`/ptk/sekolah/${params.id}`);
                }, 200);
            }

            if (state.sekolah.form.error) {
                setTimeout(() => {
                    message.config({top: 150});
                    message.error('Kesalahan!', 1.5);
                }, 200)
            }
        }, 50);
    }

    render() {
        const state = this.props.state;
        const params = this.props.match.params;

        const _tingkatPendidikan = state.tingkatPendidikan.map((item, i) => {
            return (<Select.Option value={item.id} key={i}>{item.nama}</Select.Option>);
        });

        const _kota = state.kota.data.map((item, i) => {
            return (<Select.Option value={item.id} key={i}>{item.name}</Select.Option>);
        });

        const _kecamatan = state.kecamatan.data.map((item, i) => {
            return (<Select.Option value={item.id} key={i}>{item.name}</Select.Option>);
        });

        const _errorMessage = (state.sekolah.form.error) ?
            <Alert message={state.sekolah.form.error} type="error" showIcon/> : '';


        return (
            <Shell>
                <Prompt when={!this.state.clean} message="Apakah anda yakin ingin meninggalkan halaman ini?"/>
                <section className="content-header">
                    <h1>Tambah Sekolah PTK</h1>
                </section>
                <section className="content">
                    <Breadcrumb style={{marginBottom: '10px'}}>
                        <Breadcrumb.Item><Link to='/ptk'>Data PTK</Link></Breadcrumb.Item>
                        <Breadcrumb.Item>Riwayat</Breadcrumb.Item>
                        <Breadcrumb.Item><Link to={`/ptk/sekolah/${params.id}`}>Sekolah</Link></Breadcrumb.Item>
                        <Breadcrumb.Item><Link to={`/ptk/sekolah/${params.id}`}><span
                            className='text-uppercase'>{state.detail.data.nama_lengkap}</span></Link></Breadcrumb.Item>
                        <Breadcrumb.Item>Tambah</Breadcrumb.Item>
                    </Breadcrumb>

                    <div className="box box-info">
                        <div className="box-header with-border">
                            <button className="btn btn-primary" onClick={() => {
                                this._onFormSubmit();
                            }} style={{marginRight: '5px'}} disabled={state.sekolah.form.sending}>Simpan</button>
                            <button className="btn btn-default" onClick={() => {
                                this.props.goBack();
                            }}>Batal
                            </button>
                        </div>
                        <div className="box-body">
                            <Form>
                                <div className="row">
                                    <div className="col-md-7 col-md-offset-3" style={{marginBottom: '10px'}}>
                                        {_errorMessage}
                                    </div>
                                </div>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-3 text-right">
                                            <label>Tingkat Pendidikan</label>
                                        </div>
                                        <div className="col-md-7">
                                            <Select placeholder='PILIH TINGKAT PENDIDIKAN' onChange={value => {
                                                this.setState({clean: false});
                                                this.props.setFormSekolah('tingkatPendidikan', value);
                                            }}>
                                                {_tingkatPendidikan}
                                            </Select>
                                        </div>
                                    </div>
                                </Form.Item>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-3 text-right">
                                            <label>Nama Sekolah/Universitas</label>
                                        </div>
                                        <div className="col-md-7">
                                            <Input placeholder='NAMA SEKOLAH/UNIVERSITAS' onChange={this._setForm}
                                                   name='namaSekolah'/>
                                        </div>
                                    </div>
                                </Form.Item>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-3 text-right">
                                            <label>Jurusan</label>
                                        </div>
                                        <div className="col-md-7">
                                            <Input placeholder='NAMA JURUSAN' name='jurusan'
                                                   onChange={this._setForm}/>
                                        </div>
                                    </div>
                                </Form.Item>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-3 text-right">
                                            <label>Kota</label>
                                        </div>
                                        <div className="col-md-7 ">
                                            <Select showSearch
                                                    placeholder='PILIH ATAU CARI KOTA'
                                                    onSearch={this._onKotaSearch}
                                                    onChange={value => {
                                                        this.setState({clean: false});
                                                        this._onKotaSelect(value);
                                                    }}
                                                    filterOption={false}
                                            >
                                                {_kota}
                                            </Select>
                                        </div>
                                    </div>
                                </Form.Item>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-3 text-right">
                                            <label>Kecamatan</label>
                                        </div>
                                        <div className="col-md-7">
                                            <Select
                                                showSearch
                                                onSearch={this._onKecamatanSearch}
                                                onChange={value => {
                                                    this.setState({clean: false});
                                                    this.props.setFormSekolah('kecamatan', value);
                                                }}
                                                placeholder='PILIH ATAU CARI KECAMATAN'
                                                filterOption={false}
                                            >
                                                {_kecamatan}
                                            </Select>
                                        </div>
                                    </div>
                                </Form.Item>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-3 text-right">
                                            <label>Tahun Masuk</label>
                                        </div>
                                        <div className="col-md-2">
                                            <Input placeholder='TAHUN MASUK' name='tahunMasuk'
                                                   onChange={this._setForm}/>
                                        </div>
                                        <div className="col-md-3 text-right">
                                            <label>Tahun Lulus</label>
                                        </div>
                                        <div className="col-md-2">
                                            <Input placeholder='TAHUN LULUS' name='tahunLulus'
                                                   onChange={this._setForm}/>
                                        </div>
                                    </div>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </section>
            </Shell>
        );
    }
}

const state = state => {
    return {
        state: {
            detail: state.ptk.detail,
            tingkatPendidikan: state.ptk.tingkatPendidikan,
            kota: state.area.kota,
            kecamatan: state.area.kecamatan,
            sekolah: state.ptk.sekolah
        }
    }
};

const bindDispatcher = dispatch => bindActionCreators({
    getDetailPtk,
    goBack,
    initiateTingkatPendidikan,
    initiateKota,
    initiateKecamatan,
    fetchKota,
    fetchKecamatan,
    setFormSekolah,
    submitFormSekolah,
    push,
    resetFormSekolah
}, dispatch);

export default connect(state, bindDispatcher)(PtkSekolahTambahPage)