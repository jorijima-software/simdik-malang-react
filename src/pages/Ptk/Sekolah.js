import React, {Component} from 'react';

import Shell from '../../components/Shell';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'

import {Link} from 'react-router-dom';

import InformasiPtk from '../../components/InformasiPtk';

import {Breadcrumb, Table, Popconfirm, message} from 'antd';

import {getSekolahPtk, activateSekolahPtk,deleteSekolahPtk} from "../../modules/ptk";

const { Column, ColumnGroup } = Table;

const mr = {marginRight: '3px'};

class PtkSekolahPage extends Component {
    constructor(props) {
        super(props);
        this._activateSekolah = this._activateSekolah.bind(this);
        this._deleteSekolah = this._deleteSekolah.bind(this);
    }

    componentWillMount() {
        const params = this.props.match.params;
        this.props.getSekolahPtk(params.id);
    }


    componentDidUpdate() {
        const state = this.props.state;

        if(state.sekolah.activate.loading || state.sekolah.delete.loading) {
            message.config({top: 200});
            message.loading('Mohon tunggu...', 0);
        } else {
            message.destroy();
        }
    }


    _activateSekolah(id) {
        const params = this.props.match.params;
        this.props.activateSekolahPtk(params.id, id);
    }

    _deleteSekolah(id) {
        const params = this.props.match.params;
        this.props.deleteSekolahPtk(params.id, id);
    }

    render() {
        const state = this.props.state;
        const params = this.props.match.params;

        return (
            <Shell>
                <section className="content-header">
                    <h1>Riwayat Sekolah PTK</h1>
                </section>
                <section className="content">
                    <Breadcrumb style={{marginBottom: '10px'}}>
                    <Breadcrumb.Item><Link to={`${process.env.PUBLIC_URL}/ptk`}>Data PTK</Link></Breadcrumb.Item>
                        <Breadcrumb.Item>Riwayat</Breadcrumb.Item>
                        <Breadcrumb.Item>Sekolah</Breadcrumb.Item>
                        <Breadcrumb.Item><span className='text-uppercase'>{state.detail.data.nama_lengkap}</span></Breadcrumb.Item>
                    </Breadcrumb>

                    <InformasiPtk id={params.id}/>

                    <div className="box box-primary">
                        <div className="box-header with-border">
                            <div className="box-title">Riwayat Sekolah</div>
                        </div>
                        <div className="box-body">
                            <Link to={`${process.env.PUBLIC_URL}/ptk/${params.id}/sekolah/tambah`} className='btn btn-primary btn-sm' style={{marginBottom: '10px'}}>Tambah</Link>
                            <Table size='small' dataSource={state.sekolah.data} loading={state.sekolah.loading} rowKey={col => col.id}>
                                <Column title='Tingkat' dataIndex='tingkatPendidikan'/>
                                <Column title='Nama' dataIndex='nama' />
                                <Column title='Jurusan' dataIndex='jurusan' />
                                <ColumnGroup title='Tahun'>
                                    <Column title='Masuk' dataIndex='tahun.masuk'/>
                                    <Column title='Lulus' dataIndex='tahun.lulus'/>
                                </ColumnGroup>
                                <Column title='Kota' dataIndex='kota' />
                                <Column title='Kecamatan' dataIndex='kecamatan' />
                                <Column title='Status' dataIndex='status' render={(val) => {
                                    const renderer = val == 0 ? <span className='label label-default'>NONAKTIF</span> : <span className='label label-success'>AKTIF</span>;
                                    return renderer
                                }}/>
                                <Column title='' dataIndex='id' render={(val, col) => {
                                    return (
                                        <div className='pull-right'>
                                            <Popconfirm title="Apakah anda yakin ingin mengaktifkan sekolah ini?" onConfirm={() => {
                                                this._activateSekolah(val);
                                            }}  okText="Ya" cancelText="Tidak" placement='left'>
                                                <button className="btn btn-default btn-xs" disabled={col.status == '1'} style={mr}>Aktifkan</button>
                                            </Popconfirm>
                                            <Link to={`${process.env.PUBLIC_URL}/ptk/sekolah/${params.id}/edit/${val}`} className='btn btn-primary btn-xs' style={mr}>Edit</Link>
                                            <Popconfirm title="Apakah anda yakin ingin menghapus sekolah ini?" onConfirm={() => {
                                                this._deleteSekolah(val);
                                            }}  okText="Ya" cancelText="Tidak" placement='left'>
                                                <button className="btm btn-danger btn-xs">Hapus</button>
                                            </Popconfirm>
                                        </div>
                                    )
                                }}/>
                            </Table>
                        </div>
                    </div>
                </section>
            </Shell>
        );
    }

}

const state = state => {
    return {state: {
        sekolah: state.ptk.sekolah,
        detail: state.ptk.detail
    }}
};

const bindDispatcher = dispatch => bindActionCreators({
    getSekolahPtk,
    activateSekolahPtk,
    deleteSekolahPtk
}, dispatch);

export default connect(state, bindDispatcher)(PtkSekolahPage)