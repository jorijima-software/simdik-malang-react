import React, {Component} from 'react';

import Shell from '../../../components/Shell';

import {Breadcrumb, Form, Input, DatePicker, Select} from 'antd';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link, Prompt} from 'react-router-dom';

import {goBack, push} from 'react-router-redux';

import {initiateList, searchList} from "../../../modules/sekolah";
import {getDetailPtk, initiatePangkatGolongan, setFormPangkat} from "../../../modules/ptk";
import {initiateKota, fetchKota} from "../../../modules/area";

class PtkPangkatTambahPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            clean: true
        };

        this._setForm = this._setForm.bind(this);
        this._setSelect = this._setSelect.bind(this);
        this._onUnitKerjaSearch = this._onUnitKerjaSearch.bind(this);
        this._onKotaSearch = this._onKotaSearch.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        const params = this.props.match.params;

        this.timer = null;

        this.props.getDetailPtk(params.id);
        this.props.initiatePangkatGolongan();
        this.props.initiateList();
        this.props.initiateKota();

    }

    _setForm(e) {
        clearTimeout(this.timer);

        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.timer = setTimeout(() => {this.handleChange(name, value)}, 300);
    }

    handleChange(name, value) {
        this.setState({clean: false});
        this.props.setFormPangkat(name, value);
    }

    _setSelect(name, value) {
        this.setState({clean: false});
        this.props.setFormPangkat(name, value);
    }

    _onUnitKerjaSearch(value) {
        this.props.searchList(value);
    }

    _onKotaSearch(value) {
        this.props.fetchKota(value);
    }

    render() {
        const state = this.props.state;
        const params = this.props.match.params;

        const _jenisPns = [{id: 0, nama: 'CPNS'}, {id: 1, nama: 'PNS'}].map((item, i) => {
            return (<Select.Option key={i} value={item.id}>{item.nama}</Select.Option>)
        });

        const _pangkatGolongan = state.pangkatGolongan.map((item, i) => {
            return (<Select.Option key={i} value={item.id}>{`${item.gol}/${item.ruang}`}</Select.Option>);
        });

        const _lokasiUnitKerja = [{id: 0, nama: 'DALAM MALANG'}, {id: 1, nama: 'LUAR MALANG'}].map((item, i) => {
            return (<Select.Option key={i} value={item.id}>{item.nama}</Select.Option>);
        });

        const _unitKerja = state.unitKerja.data.map((item, i) => {
            return (<Select.Option key={i} value={item.id}>{item.nama}</Select.Option>);
        });

        const _kota = state.kota.data.map((item, i) => {
            return (<Select.Option key={i} value={item.id}>{item.name}</Select.Option>);
        });

        let _formArea = '';

        if (state.pangkat.form.area == 0) {
            _formArea = (
                <div>
                    <Form.Item>
                        <div className="row">
                            <div className="col-md-2 text-right">
                                <label>Unit Kerja</label>
                            </div>
                            <div className="col-md-8">
                                <Select
                                    showSearch
                                    onSearch={this._onUnitKerjaSearch}
                                    onChange={value => {
                                        this.setState({clean: false});
                                        this._setSelect('idSekolah', value);
                                    }}
                                    filterOption={false}
                                    placeholder='PILIH ATAU CARI UNIT KERJA'
                                >
                                    {_unitKerja}
                                </Select>
                            </div>
                        </div>
                    </Form.Item>
                </div>
            );
        } else if (state.pangkat.form.area == 1) {
            _formArea = (
                <div>
                    <Form.Item>
                        <div className="row">
                            <div className="col-md-2 text-right"><label>Kota</label></div>
                            <div className="col-md-8">
                                <Select
                                    showSearch
                                    onSearch={this._onKotaSearch}
                                    onChange={value => {
                                        this.setState({clean: false});
                                        this._setSelect('kota', value);
                                    }}
                                    filterOption={false}
                                    placeholder='PILIH ATAU CARI KOTA/KECAMATAN'
                                >
                                    {_kota}
                                </Select>
                            </div>
                        </div>
                    </Form.Item>
                    <Form.Item>
                        <div className="row">
                            <div className="col-md-2 text-right"><label>Unit Kerja</label></div>
                            <div className="col-md-8"><Input placeholder="NAMA UNIT KERJA" name='unitKerja' onChange={this._setForm}/></div>
                        </div>
                    </Form.Item>
                </div>
            )
        }


        return (
            <Shell>
                <Prompt message='Apakah anda yakin ingin meninggalkan halaman ini?' when={!this.state.clean}/>
                <section className="content-header">
                    <h1>Tambah Pangkat PTK</h1>
                </section>
                <section className="content">
                    <Breadcrumb style={{marginBottom: '10px'}}>
                        <Breadcrumb.Item><Link to='/ptk'>Data PTK</Link></Breadcrumb.Item>
                        <Breadcrumb.Item>Riwayat</Breadcrumb.Item>
                        <Breadcrumb.Item><Link to={`/ptk/pangkat/${params.id}`}>Pangkat</Link></Breadcrumb.Item>
                        <Breadcrumb.Item>
                            <Link to={`/ptk/pangkat/${params.id}`}>
                                <span className='text-uppercase'>{state.detail.data.nama_lengkap}</span>
                            </Link>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>Tambah</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="box box-primary">
                        <div className="box-header with-border">
                            <button className="btn btn-primary" style={{marginRight: '5px'}}>Simpan</button>
                            <button className="btn btn-default" onClick={() => {
                                this.props.push(`${process.env.PUBLIC_URL}/ptk/${params.id}/pangkat`);
                            }}>
                                Batal
                            </button>
                        </div>
                        <div className="box-body">
                            <Form>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-2 text-right">
                                            <label>No. SK</label>
                                        </div>
                                        <div className="col-md-8">
                                            <Input placeholder='NOMOR SK' name='noSk' onChange={this._setForm}/>
                                        </div>
                                    </div>
                                </Form.Item>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-2 text-right">
                                            <label>TMT</label>
                                        </div>
                                        <div className="col-md-8">
                                            <DatePicker format='DD/MM/YYYY'
                                                        placeholder='TMT (dd/mm/yyyy)'
                                                        onChange={value => {
                                                            this._setSelect('tmt', new Date(value.toDate()))
                                                        }}/>
                                        </div>
                                    </div>
                                </Form.Item>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-2 text-right">
                                            <label>Jenis</label>
                                        </div>
                                        <div className="col-md-8">
                                            <Select placeholder='PILIH JENIS PNS' onChange={value => {
                                                this._setSelect('jenisPns', value);
                                            }}>
                                                {_jenisPns}
                                            </Select>
                                        </div>
                                    </div>
                                </Form.Item>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-2 text-right">
                                            <label>Golongan</label>
                                        </div>
                                        <div className="col-md-8">
                                            <Select placeholder='PILIH PANGKAT GOLONGAN' onChange={value => {
                                                this._setSelect('pangkatGolongan', value);
                                            }}>
                                                {_pangkatGolongan}
                                            </Select>
                                        </div>
                                    </div>
                                </Form.Item>
                                <Form.Item>
                                    <div className="row">
                                        <div className="col-md-2 text-right">
                                            <label>Lokasi Unit Kerja</label>
                                        </div>
                                        <div className="col-md-8">
                                            <Select placeholder='PILIH LOKASI UNIT KERJA' onChange={(val) => {
                                                this._setSelect ('area', val);
                                            }}>
                                                {_lokasiUnitKerja}
                                            </Select>
                                        </div>
                                    </div>
                                </Form.Item>
                                {_formArea}
                            </Form>
                        </div>
                    </div>
                </section>
            </Shell>
        );
    }
}

const state = state => {
    return {
        state: {
            detail: state.ptk.detail,
            pangkatGolongan: state.ptk.pangkatGolongan,
            pangkat: state.ptk.pangkat,
            unitKerja: state.sekolah.list,
            kota: state.area.kota
        }
    };
};

const bindDispatcher = dispatch => bindActionCreators({
    goBack,
    push,
    getDetailPtk,
    initiatePangkatGolongan,
    setFormPangkat,
    initiateList,
    searchList,
    fetchKota,
    initiateKota
}, dispatch);

export default connect(state, bindDispatcher)(PtkPangkatTambahPage);