import React, {Component} from 'react';

import {Link, Prompt} from 'react-router-dom';
import {Tabs, Breadcrumb, Spin, Icon, message} from 'antd';

import {connect} from 'react-redux';
import {push} from 'react-router-redux'
import {bindActionCreators} from 'redux'


import {initiateList, searchList} from "../../modules/sekolah";
import {fetchBidangStudi, initiateBidangStudi, initiateJenisPegawai, setFormPtk, initAgama, submitPtk} from "../../modules/ptk";
import {initiateKecamatan, initiateKota, fetchKota, fetchKecamatan} from "../../modules/area";

import {umum, profil, rumah, sertifikasi} from './partials'

import Shell from '../../components/Shell';

const TabPane = Tabs.TabPane;

class PtkTambahPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            clean: true
        };

        this.setSelect = this.setSelect.bind(this);
        this.setForm = this.setForm.bind(this);
        this._onUnitKerjaChange = this._onUnitKerjaChange.bind(this);
        this._onKotaSearch = this._onKotaSearch.bind(this);
        this._onKecamatanSearch = this._onKecamatanSearch.bind(this);
        this._onBidangStudiSearch = this._onBidangStudiSearch.bind(this);
        this._submitForm = this._submitForm.bind(this)
    }

    componentWillMount() {
        this.timer = null;
        // initiate the function
        this.props.initiateBidangStudi();
        this.props.initiateKota();
        this.props.initiateJenisPegawai();
        this.props.initiateList();
        this.props.initAgama();
    }

    _onUnitKerjaChange(query) {
        this.props.searchList(query);
    }

    _onKotaSearch(query) {
        if (query.length > 0) {
            this.props.fetchKota(query)
        }
    }

    _onKecamatanSearch(query) {
        setTimeout(() => {
            const state = this.props.state;
            const kota = state.form.rumah.kota;

            if (query.length > 0) {
                this.props.fetchKecamatan(kota, query);
            } else {
                this.props.initiateKecamatan(kota);
            }

        }, 300);
    }

    _onBidangStudiSearch(query) {
        if (query.length > 0) {
            this.props.fetchBidangStudi(query);
        }
    }

    _submitForm() {
        const state = this.props.state
        setTimeout(() => {
            this.props.submitPtk(state.form)
        }, 100)
    }

    setSelect(which, name, value) {
        this.setState({clean: false});
        this.props.setFormPtk(which, name, value);
    }

    setForm(e) {
        clearTimeout(this.timer);

        const target = e.target;
        const value = target.value;
        const name = target.name;
        const t = name.split('.');

        setTimeout(() => {
            this.setSelect(t[0], t[1], value);
        }, 300);
    }


    componentWillReceiveProps(nextProps) {
        if(nextProps.state.form.success) {
            message.config({top: 200})
            message.success('Berhasil menambahakan data PTK', 500)
            nextProps.changePage('/ptk')
        }
    }


    render() {
        const state = this.props.state;

        if (!state.login.isLogged) this.props.changePage('/login');

        const _jenisPegawai = state.jenisPegawai;
        const _unitKerja = state.unitKerja.data;
        const _agama = state.agama;
        const _bidangStudi = state.bidangStudi.data;
        const _pola = [{key: 1, nama: 'Portofolio'}, {key: 2, nama: 'PLPG'}, {key: 3, nama: 'PPG'}];
        const _kota = state.kota.data;
        const _kecamatan = state.kecamatan.data;

        // custom component

        // export default function umum(_jenisPegawai, _unitKerja, onUnitKerjaChange, setForm, setSelect) {


        const _umum = umum(_jenisPegawai, _unitKerja, this._onUnitKerjaChange, this.setForm, this.setSelect);
        const _profil = profil(_agama, this.setForm, this.setSelect);
        const _rumah = rumah(_kota, _kecamatan, this._onKotaSearch, this._onKecamatanSearch, this.setForm, this.setSelect);
        const _sertifikasi = sertifikasi(_pola, _bidangStudi, this._onBidangStudiSearch, this.setForm, this.setSelect);

        const loading = <Icon type="loading" style={{fontSize: 20, color: '#fff'}} spin/>;
        const spinner = (state.form.sending) ? <span><Spin indicator={loading}/>&nbsp;</span> : '';

        return (
            <Shell>
                <Prompt when={!this.state.clean} message='Apakah anda yakin ingin meninggalkan halaman ini?'/>
                <section className="content-header">
                    <h1>Tambah PTK</h1>
                </section>
                <section className="content">
                    <Breadcrumb style={{marginBottom: '10px'}}>
                        <Breadcrumb.Item><Link to='/ptk'>Data PTK</Link></Breadcrumb.Item>
                        <Breadcrumb.Item>Tambah</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="box box-primary" style={{marginTop: '10px'}}>
                        <div className="box-header with-border">
                            <button className="btn btn-primary" style={{marginRight: '5px'}} onClick={this._submitForm} disabled={state.form.sending}>{spinner}Simpan</button>
                            <Link to={`${process.env.PUBLIC_URL}/ptk`} className='btn btn-default'>Batal</Link>
                        </div>
                        <div className="box-body">
                            <Tabs defaultActiveKey="1">
                                <TabPane
                                    tab={<span><span>Kepegawaian</span> <span className="text-danger">*</span></span>}
                                    key="1">
                                    {_umum}
                                </TabPane>
                                <TabPane
                                    tab={<span><span>Profil</span> <span className="text-danger">*</span></span>}
                                    key="2">
                                    {_profil}
                                </TabPane>
                                <TabPane tab="Rumah" key="3">
                                    {_rumah}
                                </TabPane>
                                <TabPane tab="Sertifikasi" key="4">
                                    {_sertifikasi}
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </section>
            </Shell>
        )
    }
}

const state = state => {
    return {
        state: {
            login: state.login,
            kota: state.area.kota,
            kecamatan: state.area.kecamatan,
            form: state.ptk.form,
            unitKerja: state.sekolah.list,
            jenisPegawai: state.ptk.jenisPegawai,
            agama: state.ptk.agama,
            bidangStudi: state.ptk.bidangStudi
        }
    }
};

const mapDispatchToProps = dispatch => bindActionCreators({
    initiateKota,
    initiateKecamatan,
    fetchKota,
    fetchKecamatan,
    initiateBidangStudi,
    initiateJenisPegawai,
    fetchBidangStudi,
    initiateList,
    searchList,
    setFormPtk,
    initAgama,
    submitPtk,
    changePage: (where) => push(where)
}, dispatch);


export default connect(
    state,
    mapDispatchToProps
)(PtkTambahPage)