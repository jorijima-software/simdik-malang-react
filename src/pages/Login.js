import React, {Component} from 'react';
import {Icon, Spin, Alert, message, Select} from 'antd';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {push} from 'react-router-redux'

import logo from '../assets/logo.png';

import Shell from '../components/Shell';

import {tryLogin, formError} from "../modules/login";

class LoginPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            level: null
        };

        this._handleInputChange = this._handleInputChange.bind(this);
    }

    _handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }


    componentWillMount() {

        const state = this.props.state;

        const loginSuccess = () => {
            message.config({top: 100});
            message.loading('Mohon tunggu...', 1);

            setTimeout(() => {
                this.props.changePage(`${process.env.PUBLIC_URL}/dashboard`);
            }, 500);
        };

        const needPhoneVerification = () => {
            message.config({top: 100});
            message.loading('Mohon tunggu...', 1);

            setTimeout(() => {
                this.props.changePage(`${process.env.PUBLIC_URL}/phone-verification`);
            }, 500);
        };

        setTimeout(() => {
            console.log(state)
            if (state.isLogged && !state.needPhoneVerification) {
                loginSuccess();
            } else if (state.isLogged && state.needPhoneVerification) {
                needPhoneVerification();
            }
        }, 500)
    }


    _onSubmit(evt) {
        evt.preventDefault();
        const {username, password, level} = this.state;
        if (!username.length > 0) {
            this.props.formError('Username wajib diisi!');
        } else if (!password.length > 0) {
            this.props.formError('Password wajib diisi!');
        } else if (!level) {
            this.props.formError('Level wajib dipilih!');
        }
        else {
            this.props.tryLogin(username, password, level);
        }
    }

    render() {
        const state = this.props.state;
        const loading = <Icon type="loading" style={{fontSize: 20, color: '#fff'}} spin/>;

        const loadingIcon = (state.isSubmitting) ? <span><Spin indicator={loading}/>&nbsp;</span> : null;

        const loginSuccess = () => {
            message.config({top: 100});
            message.loading('Mohon tunggu...', 1);

            setTimeout(() => {
                this.props.changePage(`${process.env.PUBLIC_URL}/dashboard`);
            }, 500);
        };

        const needPhoneVerification = () => {
            message.config({top: 100});
            message.loading('Mohon tunggu...', 1);

            setTimeout(() => {
                this.props.changePage(`${process.env.PUBLIC_URL}/phone-verification`);
            }, 500);
        };

        if (state.isLogged && !state.needPhoneVerification) {
            setTimeout(() => {
                loginSuccess();
            }, 500)

        } else if (state.isLogged && state.needPhoneVerification) {
            setTimeout(() => {
                needPhoneVerification();
            }, 500);
        }

        const errorBox = state.error ? (
            <div className='form-group'><Alert
                message={<span><i className='fa fa-exclamation-triangle'></i> <span>Error</span></span>}
                description={state.error}
                type="error"
            /></div>
        ) : null;

        return (
            <Shell type='login'>
                <div className="login-box">
                    <div className='row'>
                        <div className="col-sm-2">
                            <img src={logo} alt="Logo Kota Malang" width='60px'/>
                        </div>
                        <div className="col-sm-10">
                            <small>Dinas Pendidikan Kota Malang</small>
                            <div style={{
                                fontFamily: "'Product Sans', sans-serif",
                                fontSize: '24px',
                                fontWeight: '800',
                                marginBottom: '20px'
                            }}>
                                SIMDIK
                            </div>
                        </div>
                    </div>
                    <div className="login-box-body">
                        <form onSubmit={this._onSubmit.bind(this)}>
                            {errorBox}
                            <div className="form-group has-feedback">
                                <label>Username</label>
                                <input name='username' type="text" className="form-control" placeholder='Username'
                                       value={this.state.username} onChange={this._handleInputChange}/>
                                <span className="fa fa-user form-control-feedback"></span>
                            </div>
                            <div className="form-group has-feedback">
                                <label>Password</label>
                                <input name='password' type="password" className="form-control" placeholder='Password'
                                       value={this.state.password} onChange={this._handleInputChange}/>
                                <span className="fa fa-key form-control-feedback"></span>
                            </div>
                            <div className="form-group">
                                <label>Level</label>
                                <Select placeholder='Pilih Level' onChange={value => {
                                    this.setState({level: value});
                                }}>
                                    <Select.Option value='1' selected>Operator</Select.Option>
                                    <Select.Option value='0'>Admin</Select.Option>
                                    <Select.Option value='2' disabled>PTK</Select.Option>
                                </Select>
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary" type='submit'
                                        disabled={state.isSubmitting}
                                        onClick={this._onSubmit.bind(this)}>
                                    {loadingIcon}Login
                                </button>
                            </div>
                            <div className="form-group">
                                <a href={`${process.env.PUBLIC_URL}/lupa-password`} style={{marginTop: '10px'}}>Lupa
                                    Password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </Shell>
        );
    }
}

const state = (state) => {
    return {state: state.login};
};

const bindDispatch = (dispatch) => bindActionCreators({
    tryLogin,
    formError,
    changePage: (where) => push(where)
}, dispatch);


export default connect(state, bindDispatch)(LoginPage)