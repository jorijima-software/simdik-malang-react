import LoginPage from "./pages/Login";
import OperatorPage from "./pages/Operator";
import DashboardPage from "./pages/Dashboard";
import SekolahDetailPage from "./pages/Sekolah/Detail";
import BeritaDetailPage from "./pages/Berita/Detail";
import BeritaPage from "./pages/Berita";
import BackupPage from "./pages/Backup";
import RombelPage from "./pages/Rombel";
import LogoutPage from "./pages/Logout";
import SekolahPage from "./pages/Sekolah";
import GuruPage from "./pages/Guru";
// ptk
import PtkPage from "./pages/Ptk";
import PtkDetailPage from "./pages/Ptk/Detail";
import PtkTambahPage from "./pages/Ptk/Tambah";
import PtkSekolahPage from "./pages/Ptk/Sekolah"
import PtkPangkatPage from "./pages/Ptk/Pangkat"
import PtkGajiPage from "./pages/Ptk/Gaji"
import PtkSekolahTambahPage from "./pages/Ptk/Sekolah/Tambah"
import PtkPangkatTambahPage from "./pages/Ptk/Pangkat/Tambah";
import PtkEditPage from "./pages/Ptk/Edit"

import PhoneVerification from "./pages/PhoneVerification";

export const routes = [
    {path: 'login', component: LoginPage},
    {path: 'logout', component:LogoutPage},
    {path: 'phone-verification', component: PhoneVerification},

    {path: 'dashboard', component: DashboardPage},

    {path: 'rombel', component: RombelPage},

    {path: 'guru', component: GuruPage},

    {path: 'sekolah/:id/detail', component: SekolahDetailPage},
    {path: 'sekolah', component: SekolahPage},

    {path: 'ptk/:id/pangkat/tambah', component: PtkPangkatTambahPage},
    {path: 'ptk/:id/sekolah/tambah', component: PtkSekolahTambahPage},

    {path: 'ptk/:id/edit', component: PtkEditPage},
    {path: 'ptk/:id/gaji', component: PtkGajiPage},
    {path: 'ptk/:id/pangkat', component: PtkPangkatPage},
    {path: 'ptk/:id/sekolah', component: PtkSekolahPage},
    {path: 'ptk/:id/detail', component: PtkDetailPage},
    {path: 'ptk/tambah', component: PtkTambahPage},

    {path: 'ptk', component: PtkPage},

    {path: '', component: DashboardPage}
];