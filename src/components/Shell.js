import React, {Component} from 'react';

import Header from './Header';
import Sidebar from './Sidebar';

import idLocaleData from 'react-intl/locale-data/id';
import {IntlProvider, addLocaleData} from 'react-intl';

export default class Shell extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        addLocaleData(idLocaleData)
    }

    render() {
        const type = (!this.props.type && this.props.type !== 'login');

        if (!type) {
            document.body.classList.add('login-page')
        } else {
            document.body.classList.remove('login-page')
        }

        const template = type ? (
            <div className="wrapper">
                <IntlProvider locale='id'>
                    <div>
                        <Header/>
                        <Sidebar/>
                        <div className="content-wrapper" style={{minHeight: '100vh'}}>
                            {this.props.children}
                        </div>
                    </div>
                </IntlProvider>
            </div>
        ) : this.props.children;

        return (
            <div className={type ? 'skin-blue sidebar-mini' : null}
                 style={{height: '100vh'}}>
                {template}
            </div>
        );
    }
}