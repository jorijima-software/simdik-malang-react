import React, {Component} from 'react';

import logo from '../assets/logo-mini.png';

import {connect} from 'react-redux'

class Header extends Component {
    render() {
        const state = this.props.state;
        return (
            <div>
                <header className="main-header">
                    <a className="logo">
                        <div className="logo-mini">
                            <div style={{fontFamily: "'Product Sans', sans-serif", lineHeight: 1}}>
                                <b>SIMDIK</b>
                            </div>
                        </div>

                        <span className="logo-lg">
            <div className="row">
                <div className="col-sm-1 text-right">
                    <img src={logo} width="40" alt="Logo Kota Malang"/>
                </div>
                <div className="col-sm-10" style={{fontFamily: "'Product Sans', sans-serif", lineHeight: 1, marginTop: '3px'}}>
                    <span style={{fontSize: "xx-small"}}>Dinas Pendidikan Kota Malang</span> <br/>
                    <b>SIMDIK</b>
                </div>
            </div>
        </span>
                    </a>

                    <nav className="navbar navbar-static-top" role="navigation">
                        <div className="navbar-custom-menu">
                            <ul className="nav navbar-nav">

                                <li>
                                    {/*<a style="padding: 8px 0">*/}
                        {/*<span className="btn btn-danger" style="margin: 0;padding: 4px">*/}
                            {/*Batas Update Data &nbsp;*/}
                            {/*<span*/}
                                {/*style="font-weight: 800;display: inline-block; font-size: 16px; font-family: 'Roboto Mono', monospace;"*/}
                                {/*countdown='' date='January 31, 2018 23:59:59'>00 hari 00:00:00</span>*/}
                        {/*</span>*/}
                                    {/*</a>*/}
                                </li>
                                <li className="dropdown user user-menu">
                                    <a className="dropdown-toggle text-uppercase" data-toggle="dropdown">
                                        <span className="hidden-xs">{state.user.namaSekolah ? state.user.namaSekolah: state.user.username}</span>
                                    </a>
                                    {/*<ul className="dropdown-menu">*/}
                                        {/*<li className="user-footer">*/}
                                            {/*<div className="pull-left">*/}
                                                {/*<a href=""*/}
                                                   {/*className="btn btn-default btn-flat">Profile</a>*/}
                                            {/*</div>*/}
                                            {/*<div className="pull-right">*/}
                                                {/*<a href=""*/}
                                                   {/*className="btn btn-default btn-flat">Sign*/}
                                                    {/*out</a>*/}
                                            {/*</div>*/}
                                        {/*</li>*/}
                                    {/*</ul>*/}
                                </li>

                            </ul>
                        </div>
                    </nav>
                </header>

            </div>
        )
    }
}

const state = state => {
    return {
        state: {
            user: state.user
        }
    }
}

export default connect(state)(Header)