import React, {Component} from 'react';

class Statsbox extends Component {
    render() {
        const box = this.props.data.map((item, i) => {
            const colors = ['yellow', 'blue', 'aqua', 'red', 'green']
            const color = colors[Math.floor(Math.random() * colors.length)]
            const boxClass = (!item.color) ? `bg-${color}` : item.color

            return (
                <div className="col-md-3" key={i}>
                    <div className={`small-box ${boxClass}`}>
                        <div className="inner">
                            <h3 style={{color: '#fff'}}>{item.value}</h3>
                            <p>{item.name}</p>
                        </div>
                        <div className="icon">
                            <i className={`fa ${item.icon}`}/>
                        </div>
                    </div>
                </div>
            )
        })
        return <div className="row" style={{marginTop: '10px'}}>{box}</div>
    }
}

export default Statsbox