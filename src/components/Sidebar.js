import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'

const menu = [
    {to: '/', name: 'Dashboard', icon: 'fa-home', show: 1},
    {to: '/berita', name: 'Berita', icon: 'fa-newspaper-o', show: 0},
    {to: '/sekolah/profil', name: 'Profil Sekolah', icon: 'fa-university', show: 2},
    {to: '/ptk', name: 'Data PTK', icon: 'fa-users', show: 0},
    {to: '/guru', name: 'Data Guru', icon: 'fa-graduation-cap', show: 0},
    {to: '/tugas-tambahan', name: 'Data Tugas Tambahan', icon: 'fa-list', show: 2},
    {to: '/sekolah', name: 'Data Sekolah', icon: 'fa-graduation-cap', show: 1},
    {to: '/operator', name: 'Data Operator', icon: 'fa-list', show: 1},
    {to: '/rombel', name: 'Data Rombel', icon: 'fa-list-ul', show: 0},
    {to: '/backup', name: 'Backup Database', icon: 'fa-hdd-o', show: 1},
    {to: '/logout', name: 'Logout', icon: 'fa-sign-out', show: 0},
];

class Sidebar extends Component {
    render() {

        const state = this.props.state;

        const sidebar = menu.map((item, i) => {
            if (state.user.level == 0 && (item.show == 1 || item.show == 0)) {
                return (
                    <li key={i}>
                        <Link to={`${process.env.PUBLIC_URL}${item.to}`}>
                            <i className={'fa ' + item.icon}></i> <span>{item.name}</span>
                        </Link>
                    </li>
                )
            } else if(state.user.level == 1 && (item.show == 2 || item.show == 0)) {
                return (
                    <li key={i}>
                        <Link to={`${process.env.PUBLIC_URL}${item.to}`}>
                            <i className={'fa ' + item.icon}></i> <span>{item.name}</span>
                        </Link>
                    </li>
                )
            }
        });

        return (
            <div>
                <aside className="main-sidebar">
                    <section className="sidebar">
                        <ul className="sidebar-menu">
                            <li className="header">MENU</li>
                            {sidebar}
                        </ul>
                    </section>
                </aside>
            </div>
        )
    }
}

const state = state => {
    return {
        state: {
            user: state.user
        }
    }
}

export default connect(state)(Sidebar);