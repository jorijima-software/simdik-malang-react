import React, {Component} from 'react'

import {connect} from 'react-redux'

import {Spin} from 'antd'

class InformasiSekolah extends Component {
    render() {
        const state = this.props.state
        const detail = state.data ? state.data : {}

        return (

            <div className='box box-info' style={{marginTop: '10px'}}>
                <div className="box-header with-border">
                    Informasi Sekolah
                </div>
                <div className="box-body">
                    <Spin spinning={state.loading}>
                        <div className="row">
                            <div className="col-md-9">
                                <table className="table table-striped table-hover">
                                    <tbody>
                                    <tr>
                                        <th width="25%">NPSN</th>
                                        <td>{detail.npsn}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">Nama</th>
                                        <td>{detail.nama}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">Tingkat</th>
                                        <td>{detail.tingkatSekolah}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">Kecamatan</th>
                                        <td>{detail.kecamatan}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">Desa</th>
                                        <td>{detail.desa}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">Alamat</th>
                                        <td>{detail.alamat}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">No. Telp</th>
                                        <td>{detail.noTelp}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">Faximile</th>
                                        <td>{detail.faximile}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">Email</th>
                                        <td>{detail.email}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">Website</th>
                                        <td>{detail.website}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">Status</th>
                                        <td>{detail.status == 0 ? 'Swasta' : 'Negeri'}</td>
                                    </tr>
                                    <tr>
                                        <th width="25%">Geografis</th>
                                        <td>{detail.geografis == 0 ? 'Pedesaan' : ((detail.geografis == 1) ? 'Perkotaan' : 'Daerah Perbatasan')}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </Spin>
                </div>
            </div>
        )
    }
}

const state = state => {
    return {
        state: state.sekolah.detail
    }
}

export default connect(state)(InformasiSekolah)