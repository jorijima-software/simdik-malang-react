import React, {Component} from 'react';

import {Link} from 'react-router-dom';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'

import {getDetailPtk} from "../modules/ptk";

import {Spin, Tooltip} from 'antd';

class InformasiPtk extends Component{
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.getDetailPtk(this.props.id);
    }

    render() {
        const state = this.props.state;
        const kepegawaian = !state.loading && state.data.kepegawaian ? state.data.kepegawaian : {};

        return (
            <Spin spinning={state.loading}>
                <div className="box box-info">
                    <div className="box-header with-border">
                        <div className="box-title">
                            Informasi PTK
                        </div>
                    </div>
                    <div className="box-body">
                        <table className="table table-striped table-hover">
                            <tbody>
                            <tr>
                                <th width="25%" className='text-right'>NUPTK</th>
                                <td>{kepegawaian.nuptk}</td>
                            </tr>
                            <tr>
                                <th className='text-right'>Nama</th>
                                <td className='text-uppercase'>
                                    <Tooltip placement='bottom' title={`Lihat detail PTK ${kepegawaian.nama}`}>
                                    <Link to={`${process.env.PUBLIC_URL}/ptk/${this.props.id}/detail`}>{kepegawaian.nama}</Link>
                                    </Tooltip>
                                </td>
                            </tr>
                            <tr>
                                <th className='text-right'>NIP</th>
                                <td>{kepegawaian.nip ? kepegawaian.nip.baru : ''}</td>
                            </tr>
                            <tr>
                                <th className='text-right'>Jenis Pegawai</th>
                                <td className='text-uppercase'>{kepegawaian.jenisPegawai}</td>
                            </tr>
                            <tr>
                                <th className='text-right'>Unit Kerja</th>
                                <td className='text-uppercase'>{kepegawaian.unitKerja}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </Spin>
        )
    }
}

const state = state => {
    return {state: state.ptk.detail}
};

const bindDispatcher = dispatch => bindActionCreators({
    getDetailPtk
}, dispatch);

export default connect(state, bindDispatcher)(InformasiPtk);