import React, {Component} from 'react';

import {Modal, Button} from 'react-bootstrap';

class DeleteButton extends Component {

    constructor(props) {
        super(props);
        this.state = {isOpen: false};
    }

    close() {
        this.setState({isOpen: false});
    }

    open() {
        this.setState({isOpen: true});
    }

    render() {
        const title = this.props.title;
        const message = (this.props.message) ? this.props.message : 'Apakah anda yakin ingin menghapus item ini?';
        const onConfirm = this.props.onConfirm;

        return (
            <span>
                <button className='btn btn-xs btn-danger' onClick={this.open.bind(this)}>
                    <i className="fa fa-times"></i> <span>Hapus</span>
                </button>

                <Modal show={this.state.isOpen} onHide={this.close.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {message}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button bsStyle='danger' bsSize='sm' onClick={onConfirm}>
                            <i className="fa fa-check"></i> <span>Konfirmasi</span>
                        </Button>
                        <Button bsStyle='default' bsSize='sm' onClick={this.close.bind(this)}>
                            <i className="fa fa-times"></i> <span>Batal</span>
                        </Button>
                    </Modal.Footer>
                </Modal>
            </span>
        );
    }
}

export default DeleteButton;