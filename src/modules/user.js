export const SET_USER = 'user/SET_USER'
export const RESET_USER = 'user/RESET_USER'

const infoToken = localStorage.getItem('INFO_TOKEN')

const decoded = infoToken ? JSON.parse(atob(infoToken)) : null

const initialState = decoded ? {
    username: decoded.username,
    level: decoded.level,
    namaSekolah: decoded.sekolah ? decoded.sekolah.nama: null,
    operator: decoded.operator
}: {
    username: null,
    level: null,
    namaSekolah: null,
    operator: null
}

export default (state = initialState, action) => {
    switch(action.type) {
        case SET_USER:
            return {
                ...state,
                ...action.payload
            }
        case RESET_USER:
            return {
                initialState
            }
        default:
            return state
    }
}

