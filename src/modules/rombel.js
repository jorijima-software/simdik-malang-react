import {sekolah} from "../providers/Rombel";

export const FETCH = 'rombel/FETCH'
export const FETCH_SUCCESS = 'rombel/FETCH_SUCCESS'
export const FETCH_FAIL = 'rombel/FETCH_FAIL'

export const FETCH_SEKOLAH = 'rombel/FETCH_SEKOLAH'
export const FETCH_SEKOLAH_SUCCESS = 'rombel/FETCH_SEKOLAH_SUCCESS'
export const FETCH_SEKOLAH_FAIL = 'rombel/FETCH_SEKOLAH_FAIL'

const initState = {
    table: {
        loading: false,
        page: null,
        data: [],
        error: false,
        total: null
    },
    tableSekolah: {
        loading: false,
        page: null,
        data: [],
        error: false,
        total: null
    }
}

export default (state = initState, action) => {
    switch (action.type) {
        case FETCH_SEKOLAH:
            return {
                ...state,
                tableSekolah: {
                    ...state.tableSekolah,
                    loading: true
                }
            }
        case FETCH_SEKOLAH_SUCCESS:
            return {
                ...state,
                tableSekolah: {
                    ...state.tableSekolah,
                    loading: false,
                    error: false,
                    page: action.payload.page,
                    data: action.payload.data,
                    total: action.payload.total
                }
            }
        case FETCH_SEKOLAH_FAIL:
            return {
                ...state,
                tableSekolah: {
                    ...state.tableSekolah,
                    loading: false,
                    error: action.payload
                }
            }
        default:
            return state
    }
}

export const initRombelSekolah = () => {
    return dispatch => {
        dispatch({type: FETCH_SEKOLAH})
        sekolah().then(result => {
            if (!result.error) {
                dispatch({type: FETCH_SEKOLAH_SUCCESS, payload: {...result, page: 0}})
            } else {
                dispatch({type: FETCH_SEKOLAH_FAIL, payload: result})
            }
        })
    }
}
