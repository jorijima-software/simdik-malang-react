import {get, show, detail} from '../providers/Sekolah';

export const INITIATE_SEKOLAH = 'sekolah/INITIATE_SEKOLAH';
export const SEARCH_SEKOLAH = 'sekolah/SEARCH_SEKOLAH';

export const FETCH_TABLE = 'sekolah/FETCH_TABLE'
export const FETCH_TABLE_SUCCESS = 'sekolah/FETCH_TABLE_SUCCESS'
export const FETCH_TABLE_FAIL = 'sekolah/FETCH_TABLE_FAIL'

export const GET_DETAIL = 'sekolah/GET_DETAIL'
export const GET_DETAIL_SUCCESS = 'sekolah/GET_DETAIL_SUCCESS'
export const GET_DETAIL_FAIL = 'sekolah/GET_DETAIL_FAIL'

const initState = {
    table: {},
    list: {
        search: null,
        data: []
    },
    table: {
        loading: false,
        data: [],
        total: null,
        error: false
    },
    detail: {
        data: {},
        loading: false,
        error: false
    }
};

export default (state = initState, action) => {
    switch (action.type) {
        case INITIATE_SEKOLAH:
            return {
                ...state,
                list: {
                    search: null,
                    data: action.payload
                }
            };
        case SEARCH_SEKOLAH:
            return {
                ...state,
                list: {
                    search: action.payload.search,
                    data: action.payload.data
                }
            };
        case FETCH_TABLE:
            return {
                ...state,
                table: {
                    ...state.table,
                    loading: true
                }
            }
        case FETCH_TABLE_SUCCESS:
            return {
                ...state,
                table: {
                    ...state.table,
                    data: action.payload.data,
                    total: action.payload.total,
                    loading: false,
                    error: false
                }
            }
        case FETCH_TABLE_FAIL:
            return {
                ...state,
                table: {
                    ...state.table,
                    error: action.payload,
                    loading: false
                }
            }
        case GET_DETAIL:
            return {
                ...state,
                detail: {
                    ...state.detail,
                    loading: true
                }
            }
        case GET_DETAIL_SUCCESS:
            return {
                ...state,
                detail: {
                    ...state.detail,
                    loading: false,
                    error: false,
                    data: action.payload
                }
            }
        case GET_DETAIL_FAIL:
            return {
                ...state,
                detail: {
                    ...state.detail,
                    error: action.payload
                }
            }
        default:
            return state;
    }
}

export const detailSekolah = (id) => {
    return dispatch => {
        dispatch({type: GET_DETAIL})
        detail(id)
            .then(r => {
                dispatch({type: GET_DETAIL_SUCCESS, payload: r.data})
            })
            .catch(err => {
                console.log(err)
            })
    }
}

export const initTable = () => {
    return dispatch => {
        dispatch({type: FETCH_TABLE});
        show(0, {}).then(_ => {
            if (!_.error) {
                dispatch({type: FETCH_TABLE_SUCCESS, payload: _})
            } else {
                dispatch({type: FETCH_TABLE_FAIL, payload: _.error})
            }
        })
    }
}

/**
 * Inisiasi List Sekolah
 * @returns {function(*)}
 */
export const initiateList = () => {
    return dispatch => {
        get('').then(_ => {
            dispatch({type: INITIATE_SEKOLAH, payload: _.data});
        })
    }
};

/**
 * Cari List Sekolah (from Server)
 * @param query
 * @returns {function(*)}
 */
export const searchList = (query) => {
    return dispatch => {
        get(query).then(_ => {
            dispatch({type: SEARCH_SEKOLAH, payload: {search: query, data: _.data}});
        })
    }
};