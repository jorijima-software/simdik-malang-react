import {fetch} from '../providers/Guru'

export const FETCH = 'guru/FETCH'
export const FETCH_SUCCESS = 'guru/FETCH_SUCCESS'
export const FETCH_FAIL = 'guru/FETCH_FAIL'

const initState = {
    table: {
        loading: false,
        data: [],
        total: null,
        error: false,
        page: null
    }
}

export default (state = initState, action) => {
    switch (action.type) {
        case FETCH:
            return {
                ...state,
                table: {
                    ...state.table,
                    loading: true
                }
            }
        case FETCH_SUCCESS:
            return {
                ...state,
                table: {
                    ...state.table,
                    loading: false,
                    data: action.payload.data,
                    total: action.payload.total,
                    error: false,
                    page: action.payload.page
                }
            }
        case FETCH_FAIL:
            return {
                ...state,
                table: {
                    ...state.table,
                    loading: false,
                    error: action.payload
                }
            }
        default:
            return state
    }
}

export const fetchTable = (page, params) => {
    return dispatch => {
        const start = (page - 1) * 10;
        dispatch({type: FETCH})
        fetch(start, params).then(result => {
            if (!result.error) {
                dispatch({type: FETCH_SUCCESS, payload: {...result, page: page}})
            } else {
                dispatch({type: FETCH_FAIL, payload: result})
            }
        })
    }
}

export const initTable = () => {
    return dispatch => {
        dispatch({type: FETCH})
        fetch(0, {}).then(result => {
            if (!result.error) {
                dispatch({type: FETCH_SUCCESS, payload: {...result, page: 0}})
            } else {
                dispatch({type: FETCH_FAIL, payload: result})
            }
        }).catch(err => {
            console.log(err)
        })
    }
}