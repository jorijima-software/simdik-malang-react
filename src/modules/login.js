import {login} from '../providers/Login'
import {RESET_USER, SET_USER} from "./user";

export const TRY_LOGIN = 'login/TRY_LOGIN';
export const LOGIN_SUCCESS = 'login/LOGIN_SUCCESS';
export const LOGIN_FAIL = 'login/LOGIN_FAIL';
export const LOGOUT = 'login/LOGOUT';
export const FORM_ERROR = 'login/FORM_ERROR';

const token = localStorage.getItem('TOKEN');
const infoToken = localStorage.getItem('INFO_TOKEN');

const defaultState = (localStorage.getItem('TOKEN') && localStorage.getItem('INFO_TOKEN')) ? {
    isSubmitting: false,
    isLogged: true,
    error: null,
    token: token,
} : {
    isSubmitting: false,
    isLogged: false,
    error: null,
    token: token,
}

const initialState = defaultState

export default (state = initialState, action) => {
    switch (action.type) {
        case TRY_LOGIN:
            return {
                ...state,
                isSubmitting: true
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                ...action.payload
            };
        case LOGIN_FAIL:
            return {
                ...state,
                isSubmitting: false,
                error: action.payload.error
            };
        case FORM_ERROR:
            return {
                ...state,
                isSubmitting: false,
                error: action.payload
            }
        case LOGOUT:
            return {
                ...state,
                isLogged: false
            };
        default:
            return state
    }
}

export const logout = () => {
    return dispatch => {
        localStorage.removeItem('TOKEN');
        localStorage.removeItem('INFO_TOKEN');
        dispatch({type: LOGOUT});
        dispatch({type: RESET_USER});
    }
};

export const tryLogin = (username, password, level) => {
    return dispatch => {
        dispatch({type: TRY_LOGIN});
        login(username, password, level).then(r => {
            if (r.success) {
                localStorage.setItem('TOKEN', r.token);
                localStorage.setItem('INFO_TOKEN', r.additionalToken);

                const decoded = JSON.parse(atob(r.additionalToken));

                dispatch({
                    type: SET_USER, payload: {
                        username: decoded.username,
                        level: decoded.level,
                        namaSekolah: decoded.sekolah ? decoded.sekolah.nama : null,
                        operator: decoded.operator
                    }
                })
                dispatch({type: LOGIN_SUCCESS, payload: {token: r.token, isLogged: true, isSubmitting: false}})
            } else {
                dispatch({type: LOGIN_FAIL, payload: r})
            }
        }).catch(err => {
            dispatch({type: LOGIN_FAIL, payload: {error: err}})
        })
    }
};

export const formError = (error) => {
    return dispatch => {
        dispatch({type: FORM_ERROR, payload: error})
    }
};