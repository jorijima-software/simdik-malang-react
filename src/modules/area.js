import {kota, kecamatan} from '../providers/Kota'

export const INITIATE_KOTA ='area/INITIATE_KOTA';
export const INITIATE_KECAMATAN ='area/INITIATE_KECAMATAN';
export const FETCH_KOTA ='area/FETCH_KOTA';
export const FETCH_KECAMATAN ='area/FETCH_KECAMATAN';

const initialState = {
    kota: {
        search: null,
        data: []
    },
    kecamatan: {
        search: null,
        data: [],
        kota: null
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_KOTA:
            return {
                ...state,
                kota: {
                    search: action.payload.search,
                    data: action.payload.data
                }
            };
        case INITIATE_KOTA:
            return {
                ...state,
                kota: {
                    ...state.kota,
                    data: action.payload.data,
                }
            };
        case FETCH_KECAMATAN:
            return {
                ...state,
                kecamatan: {
                    ...state.kecamatan,
                    data: action.payload.data,
                    kota: action.payload.kota,
                    search: action.payload.search
                }
            };
        case INITIATE_KECAMATAN:
            return {
                ...state,
                kecamatan: {
                    ...state.kecamatan,
                    kota: action.payload.kota,
                    data: action.payload.data
                }
            };
        default:
            return state
    }
}

export const initiateKota = () => {
    return dispatch => {
        kota('').then(r => {
            dispatch({type: INITIATE_KOTA, payload: {data: r}})
        })
    }
};

export const initiateKecamatan = (id) => {
    return dispatch => {
        kecamatan(id, '').then(r => {
            dispatch({type: INITIATE_KECAMATAN, payload: {kota: id, data: r}});
        })
    }
};

export const fetchKota = query => {
    return dispatch => {
        kota(query).then(r => {
            dispatch({type: FETCH_KOTA, payload: {data: r, search: query}})
        })
    }
};

export const fetchKecamatan = (id, query) => {
    return dispatch => {
        kecamatan(id, query).then(r => {
            dispatch({type: FETCH_KECAMATAN, payload: {data: r, kota: id, search: query }});
        })
    }
};