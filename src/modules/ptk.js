import {
    bidangStudi, jenisPegawai, showPtk, detailPtk, getSekolah, activateSekolah, deleteSekolah,
    getPangkat, deletePangkat, getGaji, getTipen, sendSekolahPtk, fetchPangkatGolongan, sendPtk,
    deletePtk as destroy,
} from "../providers/Ptk";
import {
    agama
} from "../providers/Agama";

const INITIATE_JENIS_PEGAWAI = 'ptk/INITIATE_JENIS_PEGAWAI';
const INITIATE_TINGKAT_PENDIDIKAN = 'ptk/INITIATE_TINGKAT_PENDIDIKAN';
const INITIATE_PANGKAT_GOLONGAN = 'ptk/INITIATE_PANGKAT_GOLONGAN';
const INITIATE_AGAMA = 'ptk/INITIATE_AGAMA';

const INITIATE_BIDANG_STUDI = 'ptk/INITIATE_BIDANG_STUDI';
const FETCH_BIDANG_STUDI = 'ptk/FETCH_BIDANG_STUDI';

const FETCH_PTK = 'ptk/FETCH_PTK';
const FETCH_PTK_SUCCESS = 'ptk/FETCH_PTK_SUCCESS';
const FETCH_PTK_FAIL = 'ptk/FETCH_PTK_FAIL';

const GET_PTK_DETAIL = 'ptk/GET_PTK_DETAIL';
const GET_PTK_DETAIL_SUCCESS = 'ptk/GET_PTK_DETAIL_SUCCESS';

const GET_PTK_SEKOLAH = 'ptk/GET_PTK_SEKOLAH';
const GET_PTK_SEKOLAH_SUCCESS = 'ptk/GET_PTK_SEKOLAH_SUCCESS';

const GET_PTK_PANGKAT = 'ptk/GET_PTK_PANGKAT';
const GET_PTK_PANGKAT_SUCCESS = 'ptk/GET_PTK_PANGKAT_SUCCESS';

const GET_PTK_GAJI = 'ptk/GET_PTK_GAJI';
const GET_PTK_GAJI_SUCCESS = 'ptk/GET_PTK_GAJI_SUCCESS';

const ACTIVATE_SEKOLAH = 'ptk/ACTIVATE_SEKOLAH';
const ACTIVATE_SEKOLAH_SUCCESS = 'ptk/ACTIVATE_SEKOLAH_SUCCESS';
const ACTIVATE_SEKOLAH_FAIL = 'ptk/ACTIVATE_SEKOLAH_FAIL';

const DELETE_SEKOLAH = 'ptk/DELETE_SEKOLAH';
const DELETE_SEKOLAH_SUCCESS = 'ptk/DELETE_SEKOLAH_SUCCESS';
const DELETE_SEKOLAH_FAIL = 'ptk/DELETE_SEKOLAH_FAIL';

const DELETE_PANGKAT = 'ptk/DELETE_PANGKAT';
const DELETE_PANGKAT_SUCCESS = 'ptk/DELETE_PANGKAT_SUCCESS';
const DELETE_PANGKAT_FAIL = 'ptk/DELETE_PANGKAT_FAIL';

const RESET_FORM_SEKOLAH = 'ptk/RESET_FORM_SEKOLAH';
const RESET_FORM_PANGKAT = 'ptk/RESET_FORM_PANGKAT';
const RESET_FORM_GAJI = 'ptk/RESET_FORM_GAJI';

const SET_FORM_SEKOLAH = 'ptk/SET_FORM_SEKOLAH';
const SET_FORM_PANGKAT = 'ptk/SET_FORM_PANGKAT';
const SET_FORM_PTK = 'ptk/SET_FORM_PTK';

const VERIFY_FORM_SEKOLAH = 'ptk/VERIFY_FORM_SEKOLAH';

const SEND_FORM_SEKOLAH = 'ptk/SEND_FORM_SEKOLAH';
const SEND_FORM_SEKOLAH_SUCCESS = 'ptk/SEND_FORM_SEKOLAH_SUCCESS';
const SEND_FORM_SEKOLAH_FAIL = 'ptk/SEND_FORM_SEKOLAH_FAIL';

const SEND_FORM_PTK = 'ptk/SEND_FORM_PTK'
const SEND_FORM_PTK_SUCCESS = 'ptk/SEND_FORM_PTK_SUCCESS'
const SEND_FORM_PTK_FAIL = 'ptk/SEND_FORM_PTK_FAIL'
const SEND_FORM_PTK_RESET = 'ptk/SEND_FORM_PTK_RESET'

const DELETE_PTK = 'ptk/DELETE_PTK'
const DELETE_PTK_SUCCESS = 'ptk/DELETE_PTK_SUCCESS'
const DELETE_PTK_FAIL = 'ptk/DELETE_PTK_FAIL'
const DELETE_PTK_RESET = 'ptk/DELETE_PTK_RESET'

const initialState = {
    bidangStudi: {
        search: null,
        data: []
    },
    jenisPegawai: [],
    table: {
        filter: null,
        search: null,
        data: [],
        total: 0,
        loading: false,
        page: 0,
        error: null
    },
    detail: {
        loading: false,
        data: {}
    },
    sekolah: {
        loading: false,
        data: [],
        activate: {
            loading: false,
            error: null
        },
        delete: {
            loading: false,
            error: null
        },
        form: {
            tingkatPendidikan: null,
            namaSekolah: '',
            jurusan: '',
            kota: null,
            kecamatan: null,
            tahunMasuk: '',
            tahunLulus: '',
            sending: false,
            error: null,
            success: null
        }
    },
    pangkat: {
        loading: false,
        data: [],
        delete: {
            loading: false,
            error: null
        },
        form: {
            noSk: null,
            tmt: null,
            jenisPns: null,
            pangkatGolongan: null,
            area: null,
            kota: null,
            unitKerja: null,
            idSekolah: null,
            // status
            success: false,
            error: false,
            sending: false
        }
    },
    gaji: {
        loading: false,
        data: [],
        delete: {
            loading: false,
            error: null
        }
    },
    tingkatPendidikan: [],
    pangkatGolongan: [],
    form: {
        kepegawaian: {
            nuptk: null,
            nama: null,
            gelarDepan: null,
            gelarBelakang: null,
            nipLama: null,
            nipBaru: null,
            jenisPegawai: null,
            unitKerja: null
        },
        profil: {
            tempatLahir: null,
            tanggalLahir: null,
            agama: null,
            nik: null,
            namaIbu: null
        },
        rumah: {
            kota: null,
            kecamatan: null,
            noTelp: null,
            alamat: null
        },
        sertifikasi: {
            pola: null,
            noPeserta: null,
            tahun: null,
            bidangStudi: null
        },
        sending: false,
        error: false,
        success: false
    },
    delete: {
        loading: false,
        success: false,
        error: false,
    },
    agama: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case INITIATE_JENIS_PEGAWAI:
            return {
                ...state,
                jenisPegawai: action.payload
            };
        case INITIATE_TINGKAT_PENDIDIKAN:
            return {
                ...state,
                tingkatPendidikan: action.payload
            };
        case INITIATE_BIDANG_STUDI:
            return {
                ...state,
                bidangStudi: {
                    ...state.bidangStudi,
                    data: action.payload
                }
            };
        case FETCH_BIDANG_STUDI:
            return {
                ...state,
                bidangStudi: {
                    search: action.payload.search,
                    data: action.payload.data
                }
            };
        case FETCH_PTK:
            return {
                ...state,
                table: {
                    ...state.table,
                    loading: true
                }
            };
        case FETCH_PTK_SUCCESS:
            return {
                ...state,
                table: {
                    ...state.table,
                    ...action.payload,
                    loading: false
                }
            };
        case FETCH_PTK_FAIL:
            return {
                ...state,
                table: {
                    ...state.table,
                    loading: false,
                    error: action.payload
                }
            }
        case GET_PTK_DETAIL:
            return {
                ...state,
                detail: {
                    ...state.detail,
                    loading: true
                }
            };
        case GET_PTK_DETAIL_SUCCESS:
            return {
                ...state,
                detail: {
                    loading: false,
                    data: action.payload
                }
            };
        case GET_PTK_SEKOLAH:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    loading: true
                }
            };
        case GET_PTK_SEKOLAH_SUCCESS:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    loading: false,
                    data: action.payload
                }
            };
        case GET_PTK_PANGKAT:
            return {
                ...state,
                pangkat: {
                    ...state.pangkat,
                    loading: true
                }
            };
        case GET_PTK_PANGKAT_SUCCESS:
            return {
                ...state,
                pangkat: {
                    ...state.pangkat,
                    loading: false,
                    data: action.payload
                }
            };
        case GET_PTK_GAJI:
            return {
                ...state,
                gaji: {
                    ...state.gaji,
                    loading: true
                }
            };
        case GET_PTK_GAJI_SUCCESS:
            return {
                ...state,
                gaji: {
                    ...state.gaji,
                    loading: false,
                    data: action.payload
                }
            };
        case ACTIVATE_SEKOLAH:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    activate: {
                        ...state.sekolah.activate,
                        loading: true
                    }
                }
            };
        case ACTIVATE_SEKOLAH_SUCCESS:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    activate: {
                        ...state.sekolah.activate,
                        loading: false
                    }
                }
            };
        case ACTIVATE_SEKOLAH_FAIL:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    activate: {
                        error: action.payload,
                        loading: false
                    }
                }
            };
        case DELETE_SEKOLAH:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    delete: {
                        ...state.sekolah.delete,
                        loading: true
                    }
                }
            };
        case DELETE_SEKOLAH_SUCCESS:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    delete: {
                        ...state.sekolah.delete,
                        loading: false
                    }
                }
            };
        case DELETE_SEKOLAH_FAIL:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    delete: {
                        error: action.payload,
                        loading: false
                    }
                }
            };
        case DELETE_PANGKAT:
            return {
                ...state,
                pangkat: {
                    ...state.pangkat,
                    delete: {
                        ...state.pangkat.delete,
                        loading: true
                    }
                }
            };
        case DELETE_PANGKAT_SUCCESS:
            return {
                ...state,
                pangkat: {
                    ...state.pangkat,
                    delete: {
                        ...state.pangkat.delete,
                        loading: false
                    }
                }
            };
        case DELETE_PANGKAT_FAIL:
            return {
                ...state,
                pangkat: {
                    ...state.pangkat,
                    delete: {
                        error: action.payload,
                        loading: false
                    }
                }
            };
        case SET_FORM_SEKOLAH:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    form: {
                        ...state.sekolah.form,
                        error: null,
                        [action.payload.name]: action.payload.value
                    }
                }
            };
        case SET_FORM_PANGKAT:
            return {
                ...state,
                pangkat: {
                    ...state.pangkat,
                    form: {
                        ...state.pangkat.form,
                        error: null,
                        [action.payload.name]: action.payload.value
                    }
                }
            };
        case SEND_FORM_SEKOLAH:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    form: {
                        ...state.sekolah.form,
                        sending: true,
                        error: null,
                        success: null
                    }
                }
            };
        case SEND_FORM_SEKOLAH_SUCCESS:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    form: {
                        ...initialState.sekolah.form,
                        sending: false,
                        success: true
                    }
                }
            };
        case SEND_FORM_SEKOLAH_FAIL:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    form: {
                        ...state.sekolah.form,
                        sending: false,
                        success: false,
                        error: action.payload
                    }
                }
            };
        case VERIFY_FORM_SEKOLAH:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    form: {
                        ...state.sekolah.form,
                        error: action.payload
                    }
                }
            };
        case RESET_FORM_SEKOLAH:
            return {
                ...state,
                sekolah: {
                    ...state.sekolah,
                    form: initialState.sekolah.form
                }
            };
        case INITIATE_PANGKAT_GOLONGAN:
            return {
                ...state,
                pangkatGolongan: action.payload
            };
        case SET_FORM_PTK:
            return {
                ...state,
                form: {
                    ...state.form,
                    [action.payload.which]: {
                        ...state.form[action.payload.which],
                        [action.payload.name]: action.payload.value
                    }
                }
            };
        case INITIATE_AGAMA:
            return {
                ...state,
                agama: action.payload
            };
        case SEND_FORM_PTK:
            return {
                ...state,
                form: {
                    ...state.form,
                    sending: true
                }
            }
        case SEND_FORM_PTK_SUCCESS:
            return {
                ...state,
                form: {
                    ...state.form,
                    sending: false,
                    success: true
                }
            }
        case SEND_FORM_PTK_FAIL:
            return {
                ...state,
                form: {
                    ...state.form,
                    sending: false,
                    error: action.payload,
                }
            }
        case SEND_FORM_PTK_RESET:
            return {
                ...state,
                form: initialState.form
            }
        case DELETE_PTK:
            return {
                ...state,
                delete: {
                    ...state.delete,
                    loading: true
                }
            }
        case DELETE_PTK_SUCCESS:
            return {
                ...state,
                delete: {
                    ...state.delete,
                    loading: false,
                    success: true
                }
            }
        case DELETE_PTK_FAIL:
            return {
                ...state,
                delete: {
                    loading: false,
                    success: false,
                    error: action.payload
                }
            }
        case DELETE_PTK_RESET:
            return {
                ...state,
                delete: initialState.delete
            }
        default:
            return state;
    }
}

export const deletePtk = (id) => {
    return dispatch => {
        dispatch({type: DELETE_PTK})
        destroy(id).then(_ => {
            if (_.error) {
                dispatch({type: DELETE_PTK_FAIL, payload: _.error})
            } else {
                dispatch({type: DELETE_PTK_SUCCESS})
                setTimeout(() => {
                    dispatch({type: DELETE_PTK_RESET})
                }, 100)
            }
        }).catch(err => {
            switch (err.status) {
                case 0:
                    dispatch({type: DELETE_PTK_FAIL, payload: 'Mengalami Kendala pada Server Utama'})
                    break;
                case 401:
                    dispatch({
                        type: DELETE_PTK_FAIL,
                        payload: 'Masa Token telah Habis, mohon coba melakukan login ulang'
                    })
                    break;
                default:
                    dispatch({type: DELETE_PTK_FAIL, payload: 'Terjadi Kendala pada saat mengkoneksikan ke Server'})
            }
        })
    }
}

export const submitPtk = (ptk) => {
    return dispatch => {
        dispatch({type: SEND_FORM_PTK})
        sendPtk(ptk).then(_ => {
            if (!_.error) {
                dispatch({type: SEND_FORM_PTK_SUCCESS})
                setTimeout(() => {
                    dispatch({type: SEND_FORM_PTK_RESET})
                }, 100)
            } else {
                dispatch({type: SEND_FORM_PTK_FAIL, payload: _.error})
            }
        }).catch(err => {
            switch (err.status) {
                case 0:
                    dispatch({type: SEND_FORM_PTK_FAIL, payload: 'Mengalami Kendala pada Server Utama'})
                    break;
                case 401:
                    dispatch({
                        type: SEND_FORM_PTK_FAIL,
                        payload: 'Masa Token telah Habis, mohon coba melakukan login ulang'
                    })
                    break;
                default:
                    dispatch({type: SEND_FORM_PTK_FAIL, payload: 'Terjadi Kendala pada saat mengkoneksikan ke Server'})
            }
        })
    }
}

export const initAgama = () => {
    return dispatch => {
        agama().then(_ => {
            dispatch({type: INITIATE_AGAMA, payload: _});
        })
    }
};

/**
 * Set Form Utama PTK
 * @param which
 * @param name
 * @param value
 * @returns {function(*)}
 */
export const setFormPtk = (which, name, value) => {
    return dispatch => {
        dispatch({type: SET_FORM_PTK, payload: {which, name, value}})
    }
};

/**
 * Set Form Pangkat
 * @param name
 * @param value
 * @returns {function(*)}
 */
export const setFormPangkat = (name, value) => {
    return dispatch => {
        dispatch({type: SET_FORM_PANGKAT, payload: {name, value}})
    }
};

/**
 * Inisiasi Pangkat Golongan
 * @returns {function(*)}
 */
export const initiatePangkatGolongan = () => {
    return dispatch => {
        fetchPangkatGolongan().then(_ => {
            dispatch({type: INITIATE_PANGKAT_GOLONGAN, payload: _});
        })
    }
};

/**
 * Reset Form Sekolah
 * @returns {function(*)}
 */
export const resetFormSekolah = () => {
    return dispatch => {
        dispatch({type: RESET_FORM_SEKOLAH});
    }
};

/**
 * Kirim Form Sekolah
 * @param id
 * @param form
 * @returns {function(*)}
 */
export const submitFormSekolah = (id, form) => {
    return dispatch => {
        if (!form.tingkatPendidikan || !form.tingkatPendidikan.length > 0) {
            dispatch({type: VERIFY_FORM_SEKOLAH, payload: 'Tingkat Pendidikan wajib dipilih!'});
        } else if (!form.namaSekolah || !form.namaSekolah.length > 0) {
            dispatch({type: VERIFY_FORM_SEKOLAH, payload: 'Nama Sekolah wajib diisi!'});
        } else if (!form.jurusan || !form.jurusan.length > 0) {
            dispatch({
                type: VERIFY_FORM_SEKOLAH,
                payload: 'Jurusan wajib diisi! (Masukkan UMUM jika tidak memiliki jurusan)'
            })
        } else if (!form.kota) {
            dispatch({type: VERIFY_FORM_SEKOLAH, payload: 'Kota wajib dipilih!'});
        } else if (!form.kecamatan) {
            dispatch({type: VERIFY_FORM_SEKOLAH, payload: 'Kecamatan wajib diisi!'});
        } else if (!form.tahunMasuk || !form.tahunMasuk.length > 0) {
            dispatch({type: VERIFY_FORM_SEKOLAH, payload: 'Tahun masuk wajib diisi!'});
        } else if (!form.tahunLulus || !form.tahunLulus.length > 0) {
            dispatch({type: VERIFY_FORM_SEKOLAH, payload: 'Tahun lulus wajib diisi!'});
        } else {
            dispatch({type: SEND_FORM_SEKOLAH});
            sendSekolahPtk(id, form).then(_ => {
                if (_.error) {
                    dispatch({type: SEND_FORM_SEKOLAH_FAIL, payload: _.error});
                } else if (_.success) {
                    dispatch({type: SEND_FORM_SEKOLAH_SUCCESS});
                } else {
                    console.log(_);
                }
            })
        }
    }
};

/**
 * Set Form Sekolah
 * @param name
 * @param value
 * @returns {function(*)}
 */
export const setFormSekolah = (name, value) => {
    return dispatch => {
        dispatch({
            type: SET_FORM_SEKOLAH, payload: {
                name, value
            }
        })
    }
};

/**
 *
 * @param id
 * @returns {function(*)}
 */
export const getGajiPtk = (id) => {
    return dispatch => {
        dispatch({type: GET_PTK_GAJI});
        getGaji(id).then(_ => {
            dispatch({type: GET_PTK_GAJI_SUCCESS, payload: _});
        });
    }
};

/**
 *
 * @returns {function(*)}
 */
export const initiateTingkatPendidikan = () => {
    return dispatch => {
        getTipen().then(_ => {
            dispatch({type: INITIATE_TINGKAT_PENDIDIKAN, payload: _});
        })
    }
};

/**
 *
 * @param id
 * @param idPangkat
 * @returns {function(*=)}
 */
export const deletePangkatPtk = (id, idPangkat) => {
    return dispatch => {
        dispatch({type: DELETE_PANGKAT});
        deletePangkat(id, idPangkat).then(_ => {
            if (_.success) {
                dispatch({type: DELETE_PANGKAT_SUCCESS});
                dispatch({type: GET_PTK_PANGKAT});
                getPangkat(id).then(_ => {
                    dispatch({type: GET_PTK_PANGKAT_SUCCESS, payload: _});
                });
            } else if (_.error) {
                dispatch({type: DELETE_SEKOLAH_FAIL, payload: _.error});
            }
        })
    }
};

/**
 *
 * @param id
 * @returns {function(*)}
 */
export const getPangkatPtk = (id) => {
    return dispatch => {
        dispatch({type: GET_PTK_PANGKAT});
        getPangkat(id).then(_ => {
            dispatch({type: GET_PTK_PANGKAT_SUCCESS, payload: _});
        });
    }
};
/**
 *
 * @param id
 * @param idSekolah
 * @returns {function(*=)}
 */
export const deleteSekolahPtk = (id, idSekolah) => {
    return dispatch => {
        dispatch({type: DELETE_SEKOLAH});
        deleteSekolah(id, idSekolah).then(_ => {
            if (_.success) {
                dispatch({type: DELETE_SEKOLAH_SUCCESS});
                dispatch({type: GET_PTK_SEKOLAH});
                getSekolah(id).then(_ => {
                    dispatch({type: GET_PTK_SEKOLAH_SUCCESS, payload: _});
                });
            } else if (_.error) {
                dispatch({type: DELETE_SEKOLAH_FAIL, payload: _.error});
            }
        })
    }
};

/**
 *
 * @param id
 * @returns {function(*)}
 */
export const getSekolahPtk = (id) => {
    return dispatch => {
        dispatch({type: GET_PTK_SEKOLAH});
        getSekolah(id).then(_ => {
            dispatch({type: GET_PTK_SEKOLAH_SUCCESS, payload: _.data});
        });
    }
};

/**
 *
 * @param id
 * @param idSekolah
 * @returns {function(*=)}
 */
export const activateSekolahPtk = (id, idSekolah) => {
    return dispatch => {
        dispatch({type: ACTIVATE_SEKOLAH});
        activateSekolah(id, idSekolah).then(_ => {
            if (_.success) {
                dispatch({type: ACTIVATE_SEKOLAH_SUCCESS});
                dispatch({type: GET_PTK_SEKOLAH});
                getSekolah(id).then(_ => {
                    dispatch({type: GET_PTK_SEKOLAH_SUCCESS, payload: _});
                });
            } else if (_.error) {
                dispatch({type: ACTIVATE_SEKOLAH_FAIL, payload: _.error});
            }
        })
    }
};


/**
 *
 * @param id
 * @returns {function(*)}
 */
export const getDetailPtk = (id) => {
    return dispatch => {
        dispatch({type: GET_PTK_DETAIL});
        detailPtk(id).then(_ => {
            dispatch({type: GET_PTK_DETAIL_SUCCESS, payload: _.data});
        })
    }
};

export const fetchPtk = (page, sort) => {
    return dispatch => {
        const start = (page - 1) * 10;

        dispatch({type: FETCH_PTK});
        showPtk(start, sort).then(_ => {
            dispatch({type: FETCH_PTK_SUCCESS, payload: {..._, page}})
        }).catch(err => {
            switch (err.status) {
                case 0:
                    dispatch({type: FETCH_PTK_FAIL, payload: 'Mengalami Kendala pada Server Utama'})
                    break;
                case 401:
                    dispatch({
                        type: FETCH_PTK_FAIL,
                        payload: 'Masa Token telah Habis, mohon coba melakukan login ulang'
                    })
                    break;
                default:
                    dispatch({type: FETCH_PTK_FAIL, payload: 'Terjadi Kendala pada saat mengkoneksikan ke Server'})
            }
        })
    }
};

/**
 *
 * @returns {function(*)}
 */
export const initiateJenisPegawai = () => {
    return dispatch => {
        jenisPegawai().then(_ => {
            dispatch({type: INITIATE_JENIS_PEGAWAI, payload: _});
        })
    }
};

/**
 *
 * @returns {function(*)}
 */
export const initiateBidangStudi = () => {
    return dispatch => {
        bidangStudi('').then(_ => {
            dispatch({type: INITIATE_BIDANG_STUDI, payload: _});
        })
    }
};

/**
 *
 * @param query
 * @returns {function(*)}
 */
export const fetchBidangStudi = (query) => {
    return dispatch => {
        bidangStudi(query).then(_ => {
            dispatch({type: FETCH_BIDANG_STUDI, payload: {search: query, data: _}});
        })
    }
};