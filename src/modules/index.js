import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'

import area from './area'
import login from './login'
import ptk from './ptk'
import sekolah from './sekolah'
import user from './user'
import guru from './guru'
import rombel from './rombel'

export default combineReducers({
    routing: routerReducer,
    area,
    login,
    ptk,
    sekolah,
    user,
    guru,
    rombel
})