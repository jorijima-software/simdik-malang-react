import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// ce es es
import 'font-awesome/css/font-awesome.min.css';
import './assets/App.css'

import registerServiceWorker from './registerServiceWorker';
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
