import React, {Component} from 'react';

import {ConnectedRouter} from 'react-router-redux'

import {
    Route,
    Switch
} from 'react-router-dom'

import {Provider} from 'react-redux'

import store, {history} from './store'

import {routes} from './routes';

class App extends Component {
    render() {
        const route = routes.map((item, i) => {
            return <Route key={i} path={`${process.env.PUBLIC_URL}/${item.path}`} component={item.component} />;
        });

        return (
            <Provider store={store}>
                <ConnectedRouter history={history} basename={'/'}>
                    <Switch>
                        {route}
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}

export default App;
