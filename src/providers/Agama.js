import axios from 'axios';

function api(params) {
    const path = localStorage.getItem('API_LOC') ? localStorage.getItem('API_LOC') : '//simdik.localhost/api/';
    return path +'/' + params.join('/');
}

/**
 * Return sets of Agama in an  array
 * @returns {Promise<any>}
 */
export function agama() {
    return new Promise((resolve, reject) => {
        axios.get(api(['agama'])).then(_ => {
            if(_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_.statusText);
            }
        });
    });
}