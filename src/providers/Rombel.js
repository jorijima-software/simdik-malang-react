import axios from 'axios'

const path = localStorage.getItem('API_LOC') ? localStorage.getItem('API_LOC') : '//simdik-api.test/api/'


export const sekolah = (start = 0, params = {}) => {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/sekolah/rombel`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('TOKEN')}`
            },
            params: {
                start: start
            }
        }).then(response => {
            return resolve(response.data)
        }).catch(err => {
            if (err.response !== undefined) {
                return reject({status: 0})
            } else {
                return reject(err.response)
            }
        })
    })
}