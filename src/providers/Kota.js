import axios from 'axios';

function api(params) {
    const path = localStorage.getItem('API_LOC') ? localStorage.getItem('API_LOC') : '//simdik.localhost/api/';
    return path + '/' + params.join('/');
}

/**
 * Return sets of Kota in an array
 * @param Pencarian
 * @returns {Promise<any>}
 */
export function kota(query) {
    return new Promise((resolve, reject) => {
        axios.get(api(['kota']), {
            params: {
                search: query
            }
        }).then(_ => {
            if(_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_.statusText);
            }
        })
    })
}

/**
 * Return sets of Kecamatan in an array
 * @param ID Kota
 * @param Pencarian
 * @returns {Promise<any>}
 */
export function kecamatan(id, query) {
    return new Promise((resolve, reject) => {
        axios.get(api(['kecamatan', id]), {
            params: {
                search: query
            }
        }).then(_ => {
            if(_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_.statusText);
            }
        })
    })
}