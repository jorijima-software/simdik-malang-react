import axios from 'axios';

const path = localStorage.getItem('API_LOC') ? localStorage.getItem('API_LOC') : '//simdik.localhost/api';

export const deletePtk = (id) => {
    return new Promise((resolve, reject) => {
        axios.delete(`${path}/ptk/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('TOKEN')}`
            }
        }).then(_ => {
            if (_.status === 200 || _.status === 201) {
                return resolve(_.data)
            } else {
                return reject(_)
            }
        }).catch(err => {
            if(err.response !== undefined) {
                return reject(err.response)
            } else {
                return reject({status: 0})
            }
        })
    })
}

export const sendPtk = (ptk) => {
    return new Promise((resolve, reject) => {
        axios.post(`${path}/ptk`, ptk, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('TOKEN')}`
            }
        }).then(_ => {
            if (_.status === 200 || _.status === 201) {
                return resolve(_);
            } else {
                return reject(_)
            }
        }).catch(err => {
            if (err.response !== undefined) {
                return reject(err.response)
            } else {
                return reject({status: 0})
            }
        })
    })
}

/**
 *
 * @returns {Promise<any>}
 */
export const fetchPangkatGolongan = () => {
    return new Promise((resolve, reject) => {
        //api/ranks
        axios.get(`${path}/ranks`).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_);
            }
        })
    })
};
/**
 *
 * @param id
 * @param form
 * @returns {Promise<any>}
 */
export const sendSekolahPtk = (id, form) => {
    return new Promise((resolve, reject) => {
        axios.post(`${path}/ptk/${id}/sekolah/tambah`, form).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_);
            }
        })
    });
};

/**
 * Get Tingkat Pendidikan
 */
export const getTipen = () => {
    return new Promise((resolve, reject) => {
        // api/pendidikan
        axios.get(`${path}/pendidikan`).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_.statusText)
            }
        }).catch(err => {
            if (err.response !== undefined) {
                return reject(err.response);
            } else {
                return reject('UNKNOWN SERVER ERR');
            }
        })
    })
};

/**
 *
 * @param id
 * @returns {Promise<any>}
 */
export const getGaji = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/teacher/${id}/salary`).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_);
            }
        })
    })
};

/**
 *
 * @param id
 * @param idPangkat
 * @returns {Promise<any>}
 */
export const deletePangkat = (id, idPangkat) => {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/teacher/${idPangkat}/rank/delete`).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_);
            }
        })
    })
};

/**
 *
 * @param id
 * @returns {Promise<any>}
 */
export const getPangkat = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/teacher/rank/${id}`).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_);
            }
        })
    })
};

/**
 *
 * @param id
 * @param idSekolah
 * @returns {Promise<any>}
 */
export const deleteSekolah = (id, idSekolah) => {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/teacher/${idSekolah}/delete/school`).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_);
            }
        })
    })
};


/**
 *
 * @param id
 * @param idSekolah
 * @returns {Promise<any>}
 */
export const activateSekolah = (id, idSekolah) => {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/teacher/${id}/activate/school/${idSekolah}`).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_);
            }
        })
    })
};

/**
 *
 * @param id
 * @returns {Promise<any>}
 */
export const getSekolah = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/ptk/${id}/sekolah`).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_);
            }
        })
    })
};

export function showPtk(start, filter) {
    return new Promise((resolve, reject) => {
        filter = filter ? filter : {};
        if (start < 0) start = 0;
        axios.get(`${path}/ptk`, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('TOKEN')
            },
            params: {
                start,
                sortField: filter.sortField,
                sortOrder: filter.sortOrder,
                nuptk: filter.nuptk,
                nama: filter.nama,
                jenisPegawai: filter.jenisPegawai,
                unitKerja: filter.unitKerja,
                search: filter.search
            }
        }).then(_ => {
            if (_.status === 200 || _.status === 201) {
                return resolve(_.data);
            } else {
                return reject(_);
            }
        }).catch(err => {
            if (err.response === undefined) {
                return reject({status: 0});
            } else {
                return reject(err.response);
            }
        });
    })
}

export function detailPtk(id) {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/ptk/${id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('TOKEN')}`
            }
        }).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_.statusText);
            }
        });
    })
}

/**
 *
 * @returns {Promise<any>}
 */
export function jenisPegawai() {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/jenis-pegawai`).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_.statusText);
            }
        });
    });
}

/**
 *
 * @param query
 * @returns {Promise<any>}
 */
export const bidangStudi = (query) => {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/bidang-studi`, {
            params: {
                search: query
            }
        }).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_.statusText);
            }
        });
    })
};

