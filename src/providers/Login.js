import axios from 'axios';

function api(params) {
    const path = localStorage.getItem('API_LOC') ? localStorage.getItem('API_LOC') : '//simdik.localhost/api';
    return path + '/'+ params.join('/');
}

export const login = (username, password, level) => {
    return new Promise((resolve, reject) => {
        axios.post(api(['user', 'login']), {
            username,
            password,
            level
        }).then(_ => {
            if(_.status === 200 || _.status === 201) {
                return resolve(_.data);
            } else {
                return reject(_.statusText);
            }
        }).catch(err => {
            if(err.response === undefined) {
                return reject('Internal Server Error');
            } else {
                return reject(err.response.statusText)
            }
        })
    });
};