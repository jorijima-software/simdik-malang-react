import axios from 'axios';

// axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('TOKEN');

const path = localStorage.getItem('API_LOC') ? localStorage.getItem('API_LOC') : '//simdik.localhost/api';


export const detail = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/sekolah/${id}`)
            .then(_ => {
                if (_.status === 200 || _.status === 201) {
                    return resolve(_.data)
                } else {
                    return reject(_)
                }
            })
            .catch(err => {
                if (err.response !== undefined) {
                    return reject(err.response)
                } else {
                    return reject({status: 0})
                }
            })
    })
}

export const show = (start, params) => {
    return new Promise((resolve, reject) => {
        axios.get(`${path}/sekolah`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('TOKEN')}`
            },
            params: {
                start
            }
        }).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_.statusText)
            }
        }).catch(err => {
            if (err.response !== undefined) {
                return reject(err.response.statusText);
            } else {
                return reject('UNKNOWN_SERVER_ERROR')
            }
        })
    })
}

export const get = (query) => {
    return new Promise((resolve, reject) => {
        //
        axios.get(`${path}/sekolah/find`, {
            params: {
                search: query
            }
        }).then(_ => {
            if (_.status === 200) {
                return resolve(_.data);
            } else {
                return reject(_);
            }
        })
    })
};